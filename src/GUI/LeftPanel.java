package GUI;

import Models.DownloadManagerBackEnd;
import Models.MainFrameTask;
import Models.GUIParameters;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * custom left panel that contains 4 custom button
 */
public class LeftPanel extends JPanel implements MouseListener {
    private GUIParameters parameters;
    private SpringLayout sp;

    private CustomButton processing;
    private CustomButton completed;
    private CustomButton Queue;
    private CustomButton removed;

    private JLabel logo;
    private ArrayList<CustomButton> panelBtns;
    private DownloadManagerBackEnd backEnd;


    public LeftPanel(GUIParameters parameter, DownloadManagerBackEnd backEnd){
        super();
        sp = new SpringLayout();
        setLayout(sp);
        this.parameters = parameter;
        this.backEnd = backEnd;
        setPreferredSize(new Dimension(parameters.getLeftPanelWidth(),parameter.getLeftPanelHeight()));
        setBackground(Color.BLACK);
        this.panelBtns = new ArrayList<>();
        setUpComponents();
    }

    private void setUpComponents(){
        this.processing = new CustomButton("./icons/download-to-storage-drive.png","Processing",parameters.getLeftPanelWidth(),55);
        this.completed = new CustomButton("./icons/circle-with-check-symbol.png","Completed",parameters.getLeftPanelWidth(),55);
        this.Queue = new CustomButton("./icons/alarm-clock.png","Queue",parameters.getLeftPanelWidth(),55);
        this.logo = new JLabel(new ImageIcon("./icons/logo.png"));
        this.removed = new CustomButton("./icons/rubbish-can.png","Removed",parameters.getLeftPanelWidth(),55);

        add(processing);
        add(completed);
        add(Queue);
        add(logo);
        add(removed);

        panelBtns.add(processing);
        panelBtns.add(Queue);
        panelBtns.add(completed);
        panelBtns.add(removed);

        setComponentSize();
        setComponentLocation();
        setListener();
    }

    private void setComponentSize(){
        logo.setPreferredSize(new Dimension(parameters.getLeftPanelWidth(),153));
        completed.setPreferredSize(new Dimension(parameters.getLeftPanelWidth(),55));
        processing.setPreferredSize(new Dimension(parameters.getLeftPanelWidth(),55));
        Queue.setPreferredSize(new Dimension(parameters.getLeftPanelWidth(),55));
        removed.setPreferredSize(new Dimension(parameters.getLeftPanelWidth(),55));
    }

    private void setComponentLocation(){
        sp.putConstraint(SpringLayout.NORTH,logo,0,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,processing,10,SpringLayout.SOUTH,logo);
        sp.putConstraint(SpringLayout.NORTH,Queue,10,SpringLayout.SOUTH,processing);
        sp.putConstraint(SpringLayout.NORTH,completed,10,SpringLayout.SOUTH,Queue);
        sp.putConstraint(SpringLayout.NORTH,removed,10,SpringLayout.SOUTH,completed);

        sp.putConstraint(SpringLayout.WEST,logo,0,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,processing,0,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,completed,0,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,Queue,0,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,removed,0,SpringLayout.WEST,this);
    }

    private void setListener(){
        for (int i = 0 ; i < panelBtns.size();++i)
            panelBtns.get(i).addMouseListener(this);
    }

    public void setSelectedPanelBorder(int index){
        if(index != -1) {
            for (int i = 0; i < panelBtns.size(); ++i) {
                if (i == index)
                    panelBtns.get(i).setBorder(BorderFactory.createLineBorder(Color.WHITE));
                else
                    panelBtns.get(i).setBorder(BorderFactory.createEmptyBorder());
            }
        }
        else
        {
            for (int i = 0; i < panelBtns.size(); ++i) {
                    panelBtns.get(i).setBorder(BorderFactory.createEmptyBorder());
            }
        }
    }
    @Override
    public void mouseClicked(MouseEvent e) {
        int index = -1;
        boolean flag = false;
        for (CustomButton di : panelBtns) {
            if (e.getSource().equals(di)) {
                index = panelBtns.indexOf(di);
                flag = true;
                break;
            }
        }
        if(flag)
            setSelectedPanelBorder(index);
        if(SwingUtilities.isLeftMouseButton(e)){
            if(e.getSource().equals(processing)){
                backEnd.setCurrentDownloadList(0);
                backEnd.updateDownloadListGUI();
            }
            else if(e.getSource().equals(Queue)){
                backEnd.setCurrentDownloadList(1);
                backEnd.updateDownloadListGUI();
            }
            else if(e.getSource().equals((completed))){
                backEnd.setCurrentDownloadList(2);
                backEnd.updateDownloadListGUI();
            }
            else if(e.getSource().equals(removed)){
                backEnd.setCurrentDownloadList(3);
                backEnd.updateDownloadListGUI();
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}

package GUI;

import javax.swing.*;
import java.awt.*;

/**
 * this class about custom button
 */
public class CustomButton extends JPanel {
    private JLabel logo;
    private JLabel text;
    private SpringLayout sp;

    public CustomButton(String path,String text,int w,int h){
        super();
        sp = new SpringLayout();
        setLayout(sp);
        logo = new JLabel(new ImageIcon(path));
        this.text = new JLabel(text);
        setPreferredSize(new Dimension(w,h+5));
        setOpaque(false);
        add(logo);
        add(this.text);
        setBorder(BorderFactory.createEmptyBorder());
        this.text.setForeground(Color.white);
        sp.putConstraint(SpringLayout.NORTH,logo,2,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,this.text,h/2,SpringLayout.NORTH,this);

        sp.putConstraint(SpringLayout.WEST,logo,10,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,this.text,h + 10,SpringLayout.EAST,logo);
    }

    public void setBorder(BorderFactory b){
        this.setBorder(b);
    }
}

package GUI;

import Models.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

/**
 * this is download item panel
 */
public class DownloadItem extends JPanel implements MouseListener, Observer {
    private GUIParameters parameters;
    private DownloadItemBackend backend;

    private JButton labelDownloadIcon;
    private JLabel labelValueDownloaded;
    private JLabel labelFileName;
    private JLabel labelProgressPercent;
    private JLabel labelSpeed;
    private JLabel startTimeLBL;
    private JLabel endTimeLBL;

    private JButton buttonInformation;
    private JButton buttonPlay;
    private JButton buttonOpenLocation;
    private JButton buttonRemove;
    private JButton buttonUp;
    private JButton buttonDown;
    private JButton addToQueue;
    private JButton restoreBTN;

    private JProgressBar progressBar;
    private DownloadManagerBackEnd btnTask;
    SpringLayout sp;
    private ArrayList<JButton> panelButtons;

    public DownloadItemBackend getBackend() {
        return backend;
    }

    public void setBackend(DownloadItemBackend backend) {
        this.backend = backend;
    }

    public DownloadItem(DownloadItemBackend db, GUIParameters para, DownloadManagerBackEnd backEnd) {
        super();
        this.parameters = para;
        this.backend = db;
        this.btnTask = backEnd;
        this.setPreferredSize(new Dimension(parameters.getDownloadItemWidth(), parameters.getDownloadItemHeight()));
        sp = new SpringLayout();
        panelButtons = new ArrayList<>();
        addMouseListener(this);
        setLayout(sp);
        setupComponents();
        setBorder(BorderFactory.createMatteBorder(1, 0, 1, 0, Color.black));
    }

    private void setupComponents() {
        addComponents();
        setComponentSize();
        setButtonTransparent();
        setButtonListener();
        setButtonBoarders(false);
        setComponentLocation();
    }

    private void addComponents() {
        labelDownloadIcon = new JButton(new ImageIcon("./icons/downloadItemIcon.png"));
        labelValueDownloaded = new JLabel("0.0MB/" + backend.getDownloadSize() + "MB");
        labelFileName = new JLabel(backend.getMetadata().getFileName());
        labelProgressPercent = new JLabel("0.0%");
        labelSpeed = new JLabel(" 0 KB/s");
        buttonInformation = new JButton(new ImageIcon("./icons/information.png"));
        buttonPlay = new JButton(new ImageIcon("./icons/playItem.png"));
        buttonOpenLocation = new JButton(new ImageIcon("./icons/open.png"));
        buttonRemove = new JButton(new ImageIcon("./icons/removeItem.png"));
        buttonUp = new JButton(new ImageIcon("./icons/up-arrow.png"));
        buttonDown = new JButton(new ImageIcon("./icons/down-arrow.png"));
        progressBar = new JProgressBar();
        addToQueue = new JButton(new ImageIcon("./icons/queue.png"));
        restoreBTN = new JButton(new ImageIcon("./icons/restore.png"));
        startTimeLBL = new JLabel(backend.getMetadata().getStartTime().toString());
        endTimeLBL = new JLabel("2018");


        add(labelDownloadIcon);
        add(labelValueDownloaded);
        add(labelFileName);
        add(labelProgressPercent);
        add(buttonInformation);
        add(buttonPlay);
        add(buttonOpenLocation);
        add(buttonRemove);
        add(progressBar);
        add(labelSpeed);
        add(buttonDown);
        add(buttonUp);
        add(addToQueue);
        add(restoreBTN);
        add(startTimeLBL);
        add(endTimeLBL);


        panelButtons.add(addToQueue);
        panelButtons.add(labelDownloadIcon);
        panelButtons.add(buttonInformation);
        panelButtons.add(buttonPlay);
        panelButtons.add(buttonOpenLocation);
        panelButtons.add(buttonRemove);
        panelButtons.add(buttonDown);
        panelButtons.add(buttonUp);
        panelButtons.add(restoreBTN);
    }

    private void setComponentSize() {
        labelDownloadIcon.setPreferredSize(new Dimension(64, 64));
        //labelValueDownloaded.setPreferredSize(new Dimension((int)(0.25 * preferredWidth),(int)(0.15 * preferredHeight)));
        //labelFileName.setPreferredSize(new Dimension((int)(0.5 * preferredWidth),(int)(0.15 * preferredHeight)));
        //labelProgressPercent.setPreferredSize(new Dimension());
        int btnSize = 20;
        buttonInformation.setPreferredSize(new Dimension(btnSize, btnSize));
        buttonPlay.setPreferredSize(new Dimension(btnSize, btnSize));
        buttonOpenLocation.setPreferredSize(new Dimension(btnSize, btnSize));
        buttonRemove.setPreferredSize(new Dimension(btnSize, btnSize));
        buttonDown.setPreferredSize(new Dimension(btnSize, btnSize));
        buttonUp.setPreferredSize(new Dimension(btnSize, btnSize));
        addToQueue.setPreferredSize(new Dimension(btnSize, btnSize));
        restoreBTN.setPreferredSize(new Dimension(btnSize, btnSize));

        int tmph = progressBar.getPreferredSize().height;
        progressBar.setPreferredSize(new Dimension((int) (0.75) * parameters.getDownloadItemWidth(), tmph));
    }

    private void setButtonTransparent() {
        for (int i = 0; i < panelButtons.size(); ++i)
            panelButtons.get(i).setContentAreaFilled(false);
    }

    private void setButtonListener() {
        labelFileName.addMouseListener(this);
        for (int i = 0; i < panelButtons.size(); ++i)
            panelButtons.get(i).addMouseListener(this);
    }

    private void setButtonBoarders(boolean state) {
        for (int i = 0; i < panelButtons.size(); ++i)
            panelButtons.get(i).setBorderPainted(state);
    }

    private void setComponentLocation() {
        int verticalGap = (int) ((0.1) * parameters.getDownloadItemHeight());
        int horizontalGap = (int) (0.0125 * parameters.getDownloadItemWidth());
        //North
        sp.putConstraint(SpringLayout.NORTH, labelDownloadIcon, 8, SpringLayout.NORTH, this);
        sp.putConstraint(SpringLayout.NORTH, labelFileName, verticalGap, SpringLayout.NORTH, this);
        sp.putConstraint(SpringLayout.NORTH, labelProgressPercent, verticalGap, SpringLayout.SOUTH, labelFileName);
        sp.putConstraint(SpringLayout.NORTH, buttonInformation, verticalGap - 2, SpringLayout.SOUTH, labelFileName);
        sp.putConstraint(SpringLayout.NORTH, progressBar, verticalGap, SpringLayout.SOUTH, labelFileName);
        sp.putConstraint(SpringLayout.NORTH, buttonOpenLocation, verticalGap, SpringLayout.SOUTH, progressBar);
        sp.putConstraint(SpringLayout.NORTH, buttonPlay, verticalGap, SpringLayout.SOUTH, progressBar);
        sp.putConstraint(SpringLayout.NORTH, buttonRemove, verticalGap, SpringLayout.SOUTH, progressBar);
        sp.putConstraint(SpringLayout.NORTH, labelValueDownloaded, verticalGap, SpringLayout.SOUTH, progressBar);
        sp.putConstraint(SpringLayout.NORTH, labelSpeed, verticalGap, SpringLayout.SOUTH, progressBar);
        sp.putConstraint(SpringLayout.NORTH, buttonUp, verticalGap, SpringLayout.SOUTH, progressBar);
        sp.putConstraint(SpringLayout.NORTH, buttonDown, verticalGap, SpringLayout.SOUTH, progressBar);
        sp.putConstraint(SpringLayout.NORTH, addToQueue, verticalGap, SpringLayout.SOUTH, progressBar);
        sp.putConstraint(SpringLayout.NORTH, restoreBTN, verticalGap, SpringLayout.SOUTH, progressBar);
        sp.putConstraint(SpringLayout.NORTH, startTimeLBL, verticalGap / 4, SpringLayout.SOUTH, progressBar);
        sp.putConstraint(SpringLayout.NORTH, endTimeLBL, verticalGap / 8, SpringLayout.SOUTH, startTimeLBL);
        //West
        int progWidth = parameters.getDownloadItemWidth() - 15 * horizontalGap - 64;
        sp.putConstraint(SpringLayout.WEST, labelDownloadIcon, horizontalGap, SpringLayout.WEST, this);
        sp.putConstraint(SpringLayout.WEST, labelFileName, (int) (horizontalGap * 2.5), SpringLayout.EAST, labelDownloadIcon);
        sp.putConstraint(SpringLayout.WEST, progressBar, (int) (horizontalGap * 2.5), SpringLayout.EAST, labelDownloadIcon);
        sp.putConstraint(SpringLayout.EAST, progressBar, progWidth, SpringLayout.WEST, progressBar);
        sp.putConstraint(SpringLayout.WEST, buttonPlay, (int) (horizontalGap * 2.5), SpringLayout.EAST, labelDownloadIcon);
        sp.putConstraint(SpringLayout.WEST, buttonOpenLocation, 5, SpringLayout.EAST, buttonPlay);
        sp.putConstraint(SpringLayout.WEST, buttonRemove, 5, SpringLayout.EAST, buttonOpenLocation);
        sp.putConstraint(SpringLayout.WEST, labelProgressPercent, 30, SpringLayout.EAST, progressBar);
        sp.putConstraint(SpringLayout.WEST, buttonInformation, 10, SpringLayout.EAST, labelProgressPercent);
        sp.putConstraint(SpringLayout.EAST, labelValueDownloaded, -15, SpringLayout.EAST, this);
        sp.putConstraint(SpringLayout.WEST, labelValueDownloaded, -1 * (labelValueDownloaded.getPreferredSize().width + 50), SpringLayout.EAST, labelValueDownloaded);
        sp.putConstraint(SpringLayout.EAST, labelSpeed, -1, SpringLayout.WEST, labelValueDownloaded);
        sp.putConstraint(SpringLayout.WEST, labelSpeed, -1 * (labelSpeed.getPreferredSize().width + 50), SpringLayout.EAST, labelSpeed);
        sp.putConstraint(SpringLayout.EAST, buttonDown, -50, SpringLayout.WEST, labelSpeed);
        sp.putConstraint(SpringLayout.WEST, buttonDown, -20, SpringLayout.EAST, buttonDown);
        sp.putConstraint(SpringLayout.EAST, buttonUp, -10, SpringLayout.WEST, buttonDown);
        sp.putConstraint(SpringLayout.WEST, buttonUp, -20, SpringLayout.EAST, buttonUp);
        sp.putConstraint(SpringLayout.EAST, addToQueue, -20, SpringLayout.WEST, buttonUp);
        sp.putConstraint(SpringLayout.EAST, restoreBTN, -20, SpringLayout.WEST, addToQueue);
        sp.putConstraint(SpringLayout.EAST, startTimeLBL, -20, SpringLayout.WEST, restoreBTN);
        sp.putConstraint(SpringLayout.EAST, endTimeLBL, -20, SpringLayout.WEST, restoreBTN);
    }

    public int getProgressPercent(){
        return progressBar.getValue();
    }

    public static String toMB(long bytes) {
        if (bytes == 0) return "";

        float mb = bytes / (1024.0f * 1024.0f);

        return String.format("%.2f MB", mb);
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        for (JButton b : panelButtons)
            if (e.getSource().equals(b))
                b.setBorderPainted(true);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        for (JButton b : panelButtons)
            if (e.getSource().equals(b))
                b.setBorderPainted(false);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof DownloadItemBackend) {
            DownloadItemBackend manager = (DownloadItemBackend) o;
            String completed = toMB(manager.getDownloadCompleted());
            if(completed.equals(""))
                completed = "0.0 MB";
            String totalSize = toMB(manager.getDownloadSize());
            labelValueDownloaded.setText(completed + " / " + totalSize);
            long percent = 0;
            long upNow = manager.getDownloadCompleted();
            long tooot = manager.getDownloadSize();
            if (tooot != 0)
                percent = (upNow * 100 / tooot);
            labelProgressPercent.setText(Long.toString(percent) + "%");
            progressBar.setValue((int) percent);
            String transferRate = toSpeed(manager.getDownloadSpeed());
            labelSpeed.setText(transferRate);
            labelFileName.setText(backend.getMetadata().getFileName());
            //String status	= manager.getStatus().name();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2 && e.getSource().equals(labelFileName)) {
            backend.openFile();

        } else if (SwingUtilities.isLeftMouseButton(e)) {
            if (e.getSource().equals(buttonInformation)) {
                DownloadInformationFrame info = new DownloadInformationFrame(parameters, backend);
            } else if (e.getSource().equals(buttonPlay))
                btnTask.resumeDownload(this);
            else if (e.getSource().equals(buttonRemove))
                btnTask.cancelDownload(this);
            else if (e.getSource().equals(labelDownloadIcon)) {
                backend.openFile();

            } else if (e.getSource().equals(buttonOpenLocation)) {
                backend.openLocation();

            } else if (e.getSource().equals(buttonUp)) {
                btnTask.moveUpInList(this);
            } else if (e.getSource().equals(buttonDown)) {
                btnTask.moveDownInLis(this);
            } else if (e.getSource().equals(addToQueue)) {
                if(this.getBackend().getDownloadListId() == 0){
                    addToQueue.setIcon(new ImageIcon("./icons/dequeue.png"));
                }
                else
                    addToQueue.setIcon(new ImageIcon("./icons/queue.png"));
                btnTask.addToQueue(this);
            }
            else if (e.getSource().equals(restoreBTN))
                btnTask.restore(this);
        } else if (SwingUtilities.isRightMouseButton(e)) {
            DownloadInformationFrame info = new DownloadInformationFrame(parameters, backend);
        }
    }

    private String toSpeed(float rate) {
        String[] SPEED = new String[]{"Bps", "KBps", "MBps", "GBps", "TBps"};

        if (rate == Float.POSITIVE_INFINITY || rate == Float.NEGATIVE_INFINITY)
            return String.format("0 %s", SPEED[0]);

        int i = 0;

        while (i < SPEED.length && rate > 1024) {
            if (rate != 0)
                rate = rate / 1024.0f;
            i++;
        }

        return String.format("%.2f %s", rate, SPEED[i]);
    }

    public void setEntTime(Date d){
        endTimeLBL.setText(d.toString());
    }

    public void setInitialVal(DownloadMetaData meta){
        this.labelFileName.setText(meta.getFileName());
        this.labelValueDownloaded.setText("0.0MB / " + toMB(meta.getFileSize()));
        this.startTimeLBL.setText(meta.getStartTime().toString());
        this.labelProgressPercent.setText("0 %");
        this.labelSpeed.setText("0 Mbs");
    }

    public void setValsAfterLoad(){
        String completed = toMB(backend.getDownloadCompleted());
        if(completed.equals(""))
            completed = "0.0 MB";
        String totalSize = toMB(backend.getDownloadSize());
        labelValueDownloaded.setText(completed + " / " + totalSize);
        long percent = 0;
        long upNow = backend.getDownloadCompleted();
        long tooot = backend.getDownloadSize();
        if (tooot != 0)
            percent = (upNow * 100 / tooot);
        labelProgressPercent.setText(Long.toString(percent) + "%");
        progressBar.setValue((int) percent);
        String transferRate = toSpeed(backend.getDownloadSpeed());
        labelSpeed.setText(transferRate);
        labelFileName.setText(backend.getMetadata().getFileName());
    }

    public void setDequeueIcon(){
        this.addToQueue.setIcon(new ImageIcon("./icons/dequeue.png"));
    }
}

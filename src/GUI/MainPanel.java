package GUI;


import Models.*;
import javax.swing.*;
import java.awt.*;

import java.io.IOException;
import java.util.ArrayList;

/**
 *main panel of main frame
 */
public class MainPanel extends JPanel {
    private CToolbar cToolBar;
    private DownloadListPanel downloadListPanel;
    private MainPanelTasks panelTasks;
    private DownloadManagerBackEnd backEnd;
    private GUIParameters parameters;
    private JScrollPane pane;

    public MainPanel(GUIParameters para, DownloadManagerBackEnd backEnd){
        super(new BorderLayout());
        this.parameters = para;
        initTasks();
        this.backEnd = backEnd;
        cToolBar= new CToolbar(parameters,panelTasks);
        downloadListPanel = new DownloadListPanel(parameters,backEnd);
        setBackground(Color.CYAN);
        setPreferredSize(new Dimension(parameters.getMainPanelWidth(),parameters.getMainPanelHeight()));
        add(cToolBar,BorderLayout.NORTH);
        pane = new JScrollPane(downloadListPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        add(pane,BorderLayout.CENTER);

    }

    public void switchDownloadList(int index){
        panelTasks.updateDownloadListGUI(index);
    }
    public void addNewDownloadFromFrame(){
        panelTasks.addNewDownload();
    }
    public void resumeDownloadFromFrame(){
        panelTasks.resumeDownload();
    }
    public void removeDownloadFromFrame(){
        panelTasks.removeDownload();
    }
    public void cancelDownload(){
        panelTasks.cancelDownload();
    }
    public void pauseFromFrame(){
        panelTasks.pauseDownload();
    }
    public void settingsFromFrame(){
        panelTasks.setting();
    }

    public DownloadListPanel getDownloadListPanel() {
        return downloadListPanel;
    }

    private void initTasks(){
        panelTasks = new MainPanelTasks() {
            @Override
            public void addNewDownload() {
                NewDownloadWindow w = new NewDownloadWindow(parameters,backEnd);
            }
            @Override
            public void removeDownload() {
                int index = downloadListPanel.getSelectedItemIndex();
                backEnd.removeDownload(index);
            }

            @Override
            public void removeDownload(DownloadItem e) {
                backEnd.removeDownload(e);
            }

            @Override
            public void cancelDownload() {
                int index = downloadListPanel.getSelectedItemIndex();
                backEnd.cancelDownload(index);
            }

            @Override
            public void cancelDownload(DownloadItem e) {
                backEnd.cancelDownload(e);
            }

            @Override
            public void pauseDownload() {
                int index = downloadListPanel.getSelectedItemIndex();
                backEnd.pauseDownload(index);
            }

            @Override
            public void setting() {
                SettingFrame sf = new SettingFrame(parameters,backEnd);

            }

            @Override
            public void moveUpInList(DownloadItem e) {
                backEnd.moveUpInList(e);
            }

            @Override
            public void moveDownInLis(DownloadItem e) {
                backEnd.moveDownInLis(e);
            }

            @Override
            public void addToQueue(DownloadItem e) {
                backEnd.addToQueue(e);
            }

            @Override
            public void restore(DownloadItem e) {
                backEnd.restore(e);
            }

            @Override
            public void search(String in) {
                backEnd.search(in);
            }

            @Override
            public void sort(int mode) {
                backEnd.sort(mode);
            }

            @Override
            public void sortOrder(int mode) {
                backEnd.sortOrder(mode);
            }

            @Override
            public void export() {
                backEnd.exportData();
            }

            @Override
            public void updateDownloadListGUI(int index) {
                backEnd.setCurrentDownloadList(index);
            }

            @Override
            public void resumeDownload() {
                int index = downloadListPanel.getSelectedItemIndex();
                backEnd.resumeDownload(index);
            }

            @Override
            public void resumeDownload(DownloadItem e) {
                backEnd.resumeDownload(e);
            }
        };
    }
}

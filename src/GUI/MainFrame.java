package GUI;

import Models.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * main frame of this programme
 */
public class MainFrame extends JFrame implements MouseListener,ActionListener,WindowListener{
    //System Tray
    private Image trayImageIcon;    // icon for Application
    private TrayIcon trayIcon;      // icon for Application in System Tray
    private SystemTray sysTray;
    private PopupMenu sysTrayMenu;  //Menu for Application in System Tray
    private MenuItem sysTrayExit;   //Exit button in system tray
    private MenuItem sysTrayOpen;   //Open button in system tray

    //Menu Bar
    private JMenuItem newDownloadItem;
    private JMenuItem pauseDownloadItem;
    private JMenuItem resumeDownloadItem;
    private JMenuItem removeDownloadItem;
    private JMenuItem cancelDownloadItem;
    private JMenuItem settingsItem;
    private JMenuItem exitItem;
    private JMenuItem helpItem;

    private MainPanel centerPanel;
    private LeftPanel lPanel;

    private GUIParameters guiParameter;

    private DownloadManagerBackEnd backEnd;

    public MainFrame() throws IOException, ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        super("JDownload Manager");

        guiParameter = new GUIParameters();
        backEnd = new DownloadManagerBackEnd(guiParameter,this);
        backEnd.loadAll();

        initComponents();
        initMenuBar();

        setIconImage(new ImageIcon("./icons/appIcon.png").getImage());
        this.setPreferredSize(new Dimension(guiParameter.getMainFrameWidth(),guiParameter.getMainFrameHeight()));
        this.setLayout(new BorderLayout());

        centerPanel = new MainPanel(guiParameter,backEnd);
        lPanel = new LeftPanel(guiParameter,backEnd);
        add(centerPanel,BorderLayout.CENTER);
        add(lPanel,BorderLayout.WEST);
        backEnd.setCurrentDownloadList(0);
        backEnd.updateDownloadListGUI();
        lPanel.setSelectedPanelBorder(0);
        backEnd.sortAllList();
        this.addWindowListener(this);
    }

    /**
     * this method provide system Tray Ability that contains two button exit and open
     * when you press system Tray Icon, Application will show also if you press open
     * when you press exit,then Application will close
     * @throws IOException
     */
    private void initComponents() throws IOException {
        if (SystemTray.isSupported()) {
           sysTray = SystemTray.getSystemTray();
           trayImageIcon  = ImageIO.read(new FileInputStream(new File("./icons/sysTrayIcon.png")));

           sysTrayMenu = new PopupMenu();

           sysTrayExit = new MenuItem("Exit");
           sysTrayOpen = new MenuItem("Open");

           sysTrayOpen.addActionListener(this);
           sysTrayExit.addActionListener(this);

           sysTrayMenu.add(sysTrayOpen);
           sysTrayMenu.add(sysTrayExit);

           trayIcon = new TrayIcon(trayImageIcon, "JDownload Manager", sysTrayMenu);
           trayIcon.addMouseListener(this);
           try {
               sysTray.add(trayIcon);
           }
           catch(AWTException e) {
               System.out.println(e.getMessage());
           }
        }
    }

    public MainPanel getCenterPanel() {
        return centerPanel;
    }

    private void initMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        JMenu downloadJMenu = new JMenu("Download");
        downloadJMenu.setMnemonic(KeyEvent.VK_D);
        JMenu helpJMenu = new JMenu("Help");

        helpJMenu.setMnemonic(KeyEvent.VK_I);

        newDownloadItem = new JMenuItem("New Download");
        newDownloadItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_N, ActionEvent.ALT_MASK));

        pauseDownloadItem =  new JMenuItem("Pause");
        pauseDownloadItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_P, ActionEvent.ALT_MASK));

        resumeDownloadItem = new JMenuItem("Resume");
        resumeDownloadItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_R, ActionEvent.ALT_MASK));

        removeDownloadItem = new JMenuItem("Remove");
        removeDownloadItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_D, ActionEvent.CTRL_MASK));

        cancelDownloadItem = new JMenuItem("Cancel");
        cancelDownloadItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_C, ActionEvent.ALT_MASK));

        settingsItem = new JMenuItem("Settings");
        settingsItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_S, ActionEvent.ALT_MASK));

        exitItem = new JMenuItem("Exit");
        exitItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_Q, ActionEvent.ALT_MASK));

        helpItem = new JMenuItem("About Us");
        helpItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_H, ActionEvent.ALT_MASK));


        newDownloadItem.addActionListener(this);
        pauseDownloadItem.addActionListener(this);
        resumeDownloadItem.addActionListener(this);
        removeDownloadItem.addActionListener(this);
        cancelDownloadItem.addActionListener(this);
        settingsItem.addActionListener(this);
        exitItem.addActionListener(this);
        helpItem.addActionListener(this);

        downloadJMenu.add(newDownloadItem);
        downloadJMenu.add(resumeDownloadItem);
        downloadJMenu.add(pauseDownloadItem);
        downloadJMenu.add(removeDownloadItem);
        downloadJMenu.add(cancelDownloadItem);
        downloadJMenu.add(settingsItem);
        downloadJMenu.add(exitItem);
        helpJMenu.add(helpItem);

        menuBar.add(downloadJMenu);
        menuBar.add(helpJMenu);
        setJMenuBar(menuBar);
    }

    public LeftPanel getlPanel() {
        return lPanel;
    }


    public void showGUI(){
        this.setVisible(true);
        this.pack();
        this.setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == sysTrayOpen)
            showGUI();
        else if(e.getSource() == exitItem || e.getSource() == sysTrayExit){
            backEnd.saveAll();
            System.exit(0);
        }

        else if(e.getSource().equals(newDownloadItem))
            centerPanel.addNewDownloadFromFrame();
        else if(e.getSource().equals(removeDownloadItem))
            centerPanel.removeDownloadFromFrame();
        else if(e.getSource().equals(resumeDownloadItem))
            centerPanel.resumeDownloadFromFrame();
        else if(e.getSource().equals(cancelDownloadItem))
            centerPanel.cancelDownload();
        else if(e.getSource().equals(pauseDownloadItem))
            centerPanel.pauseFromFrame();
        else if(e.getSource().equals(helpItem)){
            JOptionPane.showMessageDialog(null, "Developer : S.Alireza Moazeni\nStudent Number : 9423110\nStart Date: 2018-05-12\nEnd Date : 2018-05-19 "
                    , "About US", JOptionPane.INFORMATION_MESSAGE);
        }
        else if(e.getSource().equals(settingsItem))
            centerPanel.settingsFromFrame();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == trayIcon && SwingUtilities.isLeftMouseButton(e)){
            showGUI();
        }

    }
    @Override
    public void mousePressed(MouseEvent e) {

    }
    @Override
    public void mouseReleased(MouseEvent e) {

    }
    @Override
    public void mouseEntered(MouseEvent e) {

    }
    @Override
    public void mouseExited(MouseEvent e) {

    }
    @Override
    public void windowOpened(WindowEvent e) {

    }
    @Override
    public void windowClosing(WindowEvent e) {
        backEnd.getProcessingPool().removeAll();
        backEnd.getQueuePool().removeAll();

        setVisible(false);
        try { Thread.sleep(5000); }
        catch (InterruptedException ex) { }
    }
    @Override
    public void windowClosed(WindowEvent e) {}
    @Override
    public void windowIconified(WindowEvent e) {}
    @Override
    public void windowDeiconified(WindowEvent e) {}
    @Override
    public void windowActivated(WindowEvent e) {}
    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}

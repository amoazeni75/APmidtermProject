package GUI;

import Models.MainPanelTasks;
import Models.GUIParameters;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;

/**
 * this class is about custom toolbar
 */
public class CToolbar extends JPanel implements MouseListener,ActionListener {
    private JButton newDownload;
    private JButton pauseDownload;
    private JButton resumeDownload;
    private JButton cancelDownload;
    private JButton removeDownload;
    private JButton settings;
    private JButton sortMenuBTN;
    private JPopupMenu sortMenuPOP;
    private JMenuItem defaultSort;
    private JMenuItem nameSort;
    private JMenuItem dateSort;
    private JMenuItem sizeSort;
    private JMenuItem sortByIncreasing;
    private JMenuItem sortByDecreasing;
    private JButton exportBTN;
    private JButton searchBTN;
    private JTextField searchTXT;
    private GUIParameters parameters;
    private ArrayList<JButton> panelButtons;
    private MainPanelTasks buttonTasks;

    public CToolbar(GUIParameters para, MainPanelTasks tasks){
        super();
        this.parameters = para;
        this.buttonTasks = tasks;
        SpringLayout sp = new SpringLayout();
        setLayout(sp);
        int upOffset = (int)(parameters.getcTollBarHeight() * 0.1);
        int divideOffset = (int)(parameters.getcToolBarWidth() * 0.045);
        int buttonSize = (int)(parameters.getcTollBarHeight() * 0.75);
        setPreferredSize(new Dimension(parameters.getcToolBarWidth(),parameters.getcTollBarHeight()));
        panelButtons = new ArrayList<>();
        setOpaque(false);
        setupButtons(buttonSize,sp,upOffset,divideOffset);
    }
    private void setupButtons(int buttonSize, SpringLayout sp, int upOffset, int divideOffset){
        newDownload = new JButton(new ImageIcon("./icons/add.png"));
        resumeDownload = new JButton(new ImageIcon("./icons/play.png"));
        pauseDownload = new JButton(new ImageIcon("./icons/pause.png"));
        cancelDownload = new JButton(new ImageIcon("./icons/remove.png"));
        removeDownload = new JButton(new ImageIcon("./icons/taskcleaner.png"));
        settings = new JButton(new ImageIcon("./icons/settings.png"));
        sortMenuBTN = new JButton(new ImageIcon("./icons/sort.png"));
        sortMenuPOP = new JPopupMenu();
        defaultSort = new JMenuItem("Default");
        nameSort = new JMenuItem("Name");
        dateSort = new JMenuItem("Date");
        sizeSort = new JMenuItem("Size");
        sortByIncreasing = new JMenuItem("Increasing");
        sortByDecreasing = new JMenuItem("Decreasing");
        searchBTN = new JButton(new ImageIcon("./icons/loupe.png"));
        searchTXT = new JTextField("search...");
        sortMenuPOP.setLabel("Justification");
        sortMenuPOP.setBorder(new BevelBorder(BevelBorder.RAISED));
        exportBTN = new JButton(new ImageIcon("./icons/export.png"));

        panelButtons.add(newDownload);
        panelButtons.add(resumeDownload);
        panelButtons.add(pauseDownload);
        panelButtons.add(cancelDownload);
        panelButtons.add(removeDownload);
        panelButtons.add(settings);
        panelButtons.add(sortMenuBTN);
        panelButtons.add(searchBTN);
        panelButtons.add(exportBTN);

        add(newDownload);
        add(resumeDownload);
        add(pauseDownload);
        add(cancelDownload);
        add(removeDownload);
        add(settings);
        add(sortMenuBTN);
        add(searchBTN);
        add(searchTXT);
        add(exportBTN);

        sortMenuPOP.add(defaultSort);
        sortMenuPOP.add(nameSort);
        sortMenuPOP.add(dateSort);
        sortMenuPOP.add(sizeSort);
        sortMenuPOP.add(sortByIncreasing);
        sortMenuPOP.add(sortByDecreasing);

        setButtonSize(buttonSize);
        setButtonTransparent();
        setButtonListener();
        setButtonsLocations(sp,upOffset,divideOffset);
        setButtonBoarders(false);
        setToolTip();
    }
    private void setButtonsLocations(SpringLayout sp,int upOffset, int divideOffset){

        sp.putConstraint(SpringLayout.NORTH,newDownload,upOffset,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,resumeDownload,upOffset,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,pauseDownload,upOffset,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,cancelDownload,upOffset,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,removeDownload,upOffset,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,settings,upOffset,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,sortMenuBTN,upOffset,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,sortMenuBTN,upOffset,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,searchBTN,upOffset,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,searchTXT,upOffset,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,exportBTN,upOffset,SpringLayout.NORTH,this);

        sp.putConstraint(SpringLayout.WEST,newDownload,divideOffset/2,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,resumeDownload,divideOffset,SpringLayout.EAST,newDownload);
        sp.putConstraint(SpringLayout.WEST,pauseDownload,divideOffset,SpringLayout.EAST,resumeDownload);
        sp.putConstraint(SpringLayout.WEST,cancelDownload,divideOffset,SpringLayout.EAST,pauseDownload);
        sp.putConstraint(SpringLayout.WEST,removeDownload,divideOffset,SpringLayout.EAST,cancelDownload);
        sp.putConstraint(SpringLayout.WEST,sortMenuBTN,divideOffset,SpringLayout.EAST,removeDownload);
        sp.putConstraint(SpringLayout.WEST,settings,divideOffset,SpringLayout.EAST,sortMenuBTN);
        sp.putConstraint(SpringLayout.WEST,exportBTN,divideOffset,SpringLayout.EAST,settings);
        sp.putConstraint(SpringLayout.WEST,searchTXT,divideOffset,SpringLayout.EAST,exportBTN);
        sp.putConstraint(SpringLayout.WEST,searchBTN,5,SpringLayout.EAST,searchTXT);
    }
    private void setButtonTransparent(){
        for (JButton jb: panelButtons) {
            jb.setContentAreaFilled(false);
        }
    }
    private void setButtonSize(int w){
        newDownload.setPreferredSize(new Dimension(w,w));
        resumeDownload.setPreferredSize(new Dimension(w,w));
        pauseDownload.setPreferredSize(new Dimension(w,w));
        cancelDownload.setPreferredSize(new Dimension(w,w));
        removeDownload.setPreferredSize(new Dimension(w,w));
        settings.setPreferredSize(new Dimension(w,w));
        sortMenuBTN.setPreferredSize(new Dimension(w,w));
        searchBTN.setPreferredSize(new Dimension(w,w));
        searchTXT.setPreferredSize(new Dimension(4*w,w));
        exportBTN.setPreferredSize(new Dimension(w,w));
    }
    private void setButtonListener(){
        for (int i = 0 ; i < panelButtons.size(); ++i){
            panelButtons.get(i).addMouseListener(this);
        }
        dateSort.addActionListener(this);
        nameSort.addActionListener(this);
        sizeSort.addActionListener(this);
        sortByIncreasing.addActionListener(this);
        sortByDecreasing.addActionListener(this);
        defaultSort.addActionListener(this);
        sortMenuPOP.removeMouseListener(this);
    }
    private void setButtonBoarders(boolean state){
        for (JButton jb: panelButtons) {
            jb.setBorderPainted(state);
        }
    }
    private void setToolTip(){
        newDownload.setToolTipText("New Download");
        resumeDownload.setToolTipText("Resume Download");
        pauseDownload.setToolTipText("Pause Download");
        cancelDownload.setToolTipText("Cancel Download");
        removeDownload.setToolTipText("Remove Download");
        settings.setToolTipText("Settings");
        sortMenuBTN.setToolTipText("Sort By...");
        searchBTN.setToolTipText("Search...");
        exportBTN.setToolTipText("Export");
    }
    @Override
    public void mouseClicked(MouseEvent e) {
        if(SwingUtilities.isLeftMouseButton(e)){
            if(e.getSource() == newDownload)
                buttonTasks.addNewDownload();
            else if(e.getSource().equals(resumeDownload))
                buttonTasks.resumeDownload();
            else if(e.getSource().equals(removeDownload))
                buttonTasks.removeDownload();
            else if(e.getSource().equals(cancelDownload))
                buttonTasks.cancelDownload();
            else if(e.getSource().equals(pauseDownload))
                buttonTasks.pauseDownload();
            else if(e.getSource().equals(settings))
                buttonTasks.setting();
            else if(e.getSource().equals(sortMenuBTN)){
                sortMenuPOP.show(this, sortMenuBTN.getX() + 20, sortMenuBTN.getY() + 20);
            }
            else if(e.getSource().equals(searchBTN))
                buttonTasks.search(searchTXT.getText());
            else if(e.getSource().equals(exportBTN)) {
                try {
                    buttonTasks.export();
                } catch (IOException e1) {
                    System.out.println("can not export");
                }
            }
        }
    }
    @Override
    public void mousePressed(MouseEvent e) {

    }
    @Override
    public void mouseReleased(MouseEvent e) {

    }
    @Override
    public void mouseEntered(MouseEvent e) {
        for (JButton b:panelButtons)
            if(e.getSource().equals(b))
                b.setBorderPainted(true);
    }
    @Override
    public void mouseExited(MouseEvent e) {
        for (JButton b:panelButtons)
            if(e.getSource().equals(b))
                b.setBorderPainted(false);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(defaultSort))
            buttonTasks.sort(2);
        else if(e.getSource().equals(sizeSort))
            buttonTasks.sort(1);
        else if(e.getSource().equals(dateSort))
            buttonTasks.sort(2);
        else if(e.getSource().equals(nameSort))
            buttonTasks.sort(0);
        else if(e.getSource().equals(sortByIncreasing))
            buttonTasks.sortOrder(0);
        else if(e.getSource().equals(sortByDecreasing))
            buttonTasks.sortOrder(1);

    }
}

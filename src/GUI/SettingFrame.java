package GUI;

import Models.DownloadManagerBackEnd;
import Models.GUIParameters;
import Models.ProgramSettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Vector;

public class SettingFrame extends JFrame implements MouseListener,ActionListener {
    private SpringLayout sp;
    private GUIParameters parameters;

    private JLabel parallelIcon;
    private JTextField parallelTextField;

    private JLabel connectionPerDownloadLBL;
    private JComboBox<String> connectionCountCOM;

    private JLabel lookAndFeelMsg;
    private JButton selectLocationBtn;
    private JTextField locationTxt;

    private JTextField filterUrlTF;
    private JButton filterOkBTN;
    private JLabel filterLBL;
    private JComboBox<String> filterCOM;
    private JButton editFilterBTN;
    private JButton deleteFilterUrl;
    private JButton okExit;

    private JPanel groupBtn;
    private ArrayList<JRadioButton> lookOptions;
    private ArrayList<JButton> panelBTNs;
    private DownloadManagerBackEnd backEnd;
    private boolean allowAdd;

    public SettingFrame(GUIParameters para, DownloadManagerBackEnd backEnd){
        super("Settings");
        this.parameters = para;
        sp = new SpringLayout();
        setLayout(sp);
        setPreferredSize(new Dimension(parameters.getSettingFrameWidth(),parameters.getSettingFrameHeight()));
        this.setResizable(false);
        this.backEnd = backEnd;
        setUpComponents();
        this.pack();
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setVisible(true);
        allowAdd = true;
    }

    private void setUpComponents(){
        parallelIcon = new JLabel(new ImageIcon("./icons/parallel.png"));
        parallelTextField = new JTextField(ProgramSettings.parallelDownloadCount);
        parallelTextField.setText(Integer.toString(backEnd.getProgramSettings().getParallelDownloadCount()));
        selectLocationBtn = new JButton(new ImageIcon("./icons/selectLocation.png"));
        locationTxt = new JTextField(ProgramSettings.getDefaultLocation());
        groupBtn = new JPanel(new GridLayout(backEnd.getProgramSettings().getLookAndFeelsCount(),1,3,3));
        lookOptions = new ArrayList<>();
        lookAndFeelMsg = new JLabel("Look And Feels :");
        for (int i = 0; i< backEnd.getProgramSettings().getLookAndFeelsCount(); ++i){
            JRadioButton tmp = new JRadioButton(ProgramSettings.getLookAndFeels(i).getName());
            groupBtn.add(tmp);
            lookOptions.add(tmp);
        }
        for (int i = 0;  i< backEnd.getProgramSettings().getLookAndFeelsCount(); ++i){
            String name = ProgramSettings.getCurrentLookName();
            if(lookOptions.get(i).getText().equals(name)){
                lookOptions.get(i).setSelected(true);
            }
        }

        panelBTNs = new ArrayList<>();
        filterUrlTF = new JTextField();
        filterOkBTN = new JButton(new ImageIcon("./icons/ok32.png"));
        filterLBL = new JLabel(new ImageIcon("./icons/firewall.png"));
        filterCOM = new JComboBox<String>();
        deleteFilterUrl = new JButton(new ImageIcon("./icons/delete32.png"));
        ArrayList<String> tempCombo = backEnd.getFilterUrl();
        for (int i = 0; i < tempCombo.size(); i++) {
            filterCOM.addItem(tempCombo.get(i));
        }
        okExit = new JButton(new ImageIcon("./icons/ok.png"));
        editFilterBTN = new JButton(new ImageIcon("./icons/edit.png"));

        connectionPerDownloadLBL = new JLabel("Connection Per Download : ");
        connectionCountCOM = new JComboBox<>();
        connectionCountCOM.addItem("1");
        connectionCountCOM.addItem("2");
        connectionCountCOM.addItem("4");
        connectionCountCOM.addItem("8");
        connectionCountCOM.addItem("16");

        add(parallelIcon);
        add(parallelTextField);
        add(selectLocationBtn);
        add(locationTxt);
        add(groupBtn);
        add(lookAndFeelMsg);
        add(filterCOM);
        add(filterLBL);
        add(filterOkBTN);
        add(filterUrlTF);
        add(okExit);
        add(editFilterBTN);
        add(deleteFilterUrl);
        add(connectionPerDownloadLBL);
        add(connectionCountCOM);

        panelBTNs.add(filterOkBTN);
        panelBTNs.add(selectLocationBtn);
        panelBTNs.add(okExit);
        panelBTNs.add(editFilterBTN);
        panelBTNs.add(deleteFilterUrl);

        setComponentSize();
        setButtonsTransparent();
        setButtonListener();
        setButtonBoarders(false);
        setComponentsLocation();

    }
    private void setComponentSize(){
        int iconSize = 32;
        int horizontalGap = (int)(0.1 * parameters.getNewWindowDownloadWidth());
        int iconTextGap = 10;
        int textFieldWidth = (parameters.getNewWindowDownloadWidth() - iconSize - 2 * horizontalGap - iconTextGap );
        parallelIcon.setPreferredSize(new Dimension(iconSize,iconSize));
        selectLocationBtn.setPreferredSize(new Dimension(iconSize,iconSize));
        locationTxt.setPreferredSize(new Dimension(textFieldWidth,locationTxt.getPreferredSize().height));
        filterLBL.setPreferredSize(new Dimension(iconSize,iconSize));
        filterUrlTF.setPreferredSize(new Dimension(textFieldWidth,filterUrlTF.getPreferredSize().height));
        filterOkBTN.setPreferredSize(new Dimension(iconSize,iconSize));
        filterCOM.setPreferredSize(new Dimension((int)(textFieldWidth * 0.8),filterCOM.getPreferredSize().height));
        okExit.setPreferredSize(new Dimension(2 *iconSize,2 *iconSize));
        editFilterBTN.setPreferredSize(new Dimension(iconSize,iconSize));
        deleteFilterUrl.setPreferredSize(new Dimension(iconSize,iconSize));
    }
    private void setButtonsTransparent(){
        for (int i = 0; i < lookOptions.size(); ++i)
            lookOptions.get(i).setContentAreaFilled(false);
        for (int i = 0; i < panelBTNs.size(); i++) {
            panelBTNs.get(i).setContentAreaFilled(false);
        }
    }
    private void setButtonListener(){
        for (int i = 0; i < lookOptions.size(); ++i)
            lookOptions.get(i).addMouseListener(this);
        for (int i = 0; i < panelBTNs.size(); ++i)
            panelBTNs.get(i).addMouseListener(this);
        filterCOM.addActionListener(this);
    }
    private void setButtonBoarders(boolean status){
        for (int i = 0; i < lookOptions.size(); ++i)
            lookOptions.get(i).setBorderPainted(status);
        for (int i = 0; i < panelBTNs.size(); ++i)
            panelBTNs.get(i).setBorderPainted(status);
    }
    private void setComponentsLocation(){
        int horizontalGap = 20;
        int verticalGap = 20;
        sp.putConstraint(SpringLayout.NORTH,parallelIcon,20,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,connectionPerDownloadLBL,8,SpringLayout.NORTH,parallelIcon);
        sp.putConstraint(SpringLayout.NORTH,connectionCountCOM,8,SpringLayout.NORTH,parallelIcon);
        sp.putConstraint(SpringLayout.NORTH,parallelTextField,6,SpringLayout.NORTH,parallelIcon);
        sp.putConstraint(SpringLayout.NORTH,selectLocationBtn,verticalGap,SpringLayout.SOUTH,parallelIcon);
        sp.putConstraint(SpringLayout.NORTH,locationTxt,6,SpringLayout.NORTH,selectLocationBtn);
        sp.putConstraint(SpringLayout.NORTH,lookAndFeelMsg,20,SpringLayout.SOUTH,selectLocationBtn);
        sp.putConstraint(SpringLayout.NORTH,groupBtn,10,SpringLayout.SOUTH,lookAndFeelMsg);
        sp.putConstraint(SpringLayout.NORTH,filterLBL,20,SpringLayout.SOUTH,groupBtn);
        sp.putConstraint(SpringLayout.NORTH,filterUrlTF,26,SpringLayout.SOUTH,groupBtn);
        sp.putConstraint(SpringLayout.NORTH,filterOkBTN,20,SpringLayout.SOUTH,groupBtn);
        sp.putConstraint(SpringLayout.NORTH,filterCOM,6,SpringLayout.SOUTH,filterLBL);
        sp.putConstraint(SpringLayout.SOUTH,okExit,-40,SpringLayout.SOUTH,this);
        sp.putConstraint(SpringLayout.NORTH,okExit,-64,SpringLayout.SOUTH,okExit);
        sp.putConstraint(SpringLayout.NORTH,editFilterBTN,20,SpringLayout.SOUTH,groupBtn);
        sp.putConstraint(SpringLayout.NORTH,deleteFilterUrl,20,SpringLayout.SOUTH,groupBtn);

        sp.putConstraint(SpringLayout.WEST,parallelIcon,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,connectionPerDownloadLBL,50,SpringLayout.EAST,parallelTextField);
        sp.putConstraint(SpringLayout.WEST,connectionCountCOM,5,SpringLayout.EAST,connectionPerDownloadLBL);
        sp.putConstraint(SpringLayout.WEST,parallelTextField,10,SpringLayout.EAST,parallelIcon);
        sp.putConstraint(SpringLayout.EAST,parallelTextField,50,SpringLayout.WEST,parallelTextField);
        sp.putConstraint(SpringLayout.WEST,selectLocationBtn,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,locationTxt,10,SpringLayout.EAST,selectLocationBtn);
        sp.putConstraint(SpringLayout.WEST,groupBtn,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,lookAndFeelMsg,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,filterLBL,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,filterUrlTF,10,SpringLayout.EAST,filterLBL);
        sp.putConstraint(SpringLayout.WEST,filterOkBTN,10,SpringLayout.EAST,filterUrlTF);
        sp.putConstraint(SpringLayout.WEST,filterCOM,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.EAST,okExit,-20,SpringLayout.EAST,this);
        sp.putConstraint(SpringLayout.WEST,okExit,-64,SpringLayout.EAST,okExit);
        sp.putConstraint(SpringLayout.WEST,editFilterBTN,10,SpringLayout.EAST,filterOkBTN);
        sp.putConstraint(SpringLayout.WEST,deleteFilterUrl,10,SpringLayout.EAST,editFilterBTN);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(SwingUtilities.isLeftMouseButton(e)) {
            if (e.getSource().equals(selectLocationBtn)) {
                JFileChooser fc = new JFileChooser();
                fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                    locationTxt.setText(fc.getSelectedFile().getAbsolutePath());
                }
            }
            else if (lookOptions.contains(e.getSource())) {
                int index = lookOptions.indexOf(e.getSource());
                lookOptions.get(index).setSelected(true);
                try {
                    UIManager.setLookAndFeel(ProgramSettings.getLookAndFeels(index).getClassName());//lookOptions.get(index).getText());
                    backEnd.getProgramSettings().setCurrentLook(ProgramSettings.getLookAndFeels(index));
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                } catch (InstantiationException e1) {
                    e1.printStackTrace();
                } catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                } catch (UnsupportedLookAndFeelException e1) {
                    e1.printStackTrace();
                }
                for (int i = 0; i < backEnd.getProgramSettings().getLookAndFeelsCount(); i++) {
                    if (i == index)
                        continue;
                    else
                        lookOptions.get(i).setSelected(false);
                }
            }
            else if(e.getSource().equals(okExit)){
                int threadCount = 0;
                int connection = 0;
                try {
                    threadCount = Integer.parseInt(parallelTextField.getText());
                    connection = Integer.parseInt(connectionCountCOM.getSelectedItem().toString());
                }catch (NumberFormatException b){

                }
                backEnd.getProgramSettings().setParallelDownloadCount(threadCount);
                ProgramSettings.connectionCountPerDownload = connection;
                //FilterList
                ArrayList<String> tmpArr = new ArrayList<>();
                for (int i = 0; i < filterCOM.getItemCount(); ++i){
                    tmpArr.add(filterCOM.getItemAt(i));
                }
                backEnd.setNewFilterURL(tmpArr);
                //location
                backEnd.getProgramSettings().setDefaultLocation(locationTxt.getText());
                this.dispose();
            }
            else if(e.getSource().equals(filterOkBTN)){
                filterCOM.addItem(filterUrlTF.getText());
                filterUrlTF.setText("");
            }
            else if(e.getSource().equals(editFilterBTN)){
                Vector<String> ttt = new Vector<String >();
                int forbidIn = filterCOM.getSelectedIndex();
                for (int i = 0 ; i< filterCOM.getItemCount(); ++i){
                    if(i == forbidIn)
                        ttt.add(filterUrlTF.getText());
                    else
                        ttt.add(filterCOM.getItemAt(i));
                }
                allowAdd = false;
                filterUrlTF.setText("");
                filterCOM.removeAllItems();
                for (int i = 0 ; i< ttt.size(); ++i)
                    filterCOM.addItem(ttt.elementAt(i));
                allowAdd = true;
                filterCOM.updateUI();
            }
            else if(e.getSource().equals(deleteFilterUrl)){
                Vector<String> ttt = new Vector<String >();
                int forbidIn = filterCOM.getSelectedIndex();
                for (int i = 0 ; i< filterCOM.getItemCount(); ++i){
                    if(i == forbidIn)
                        continue;
                    else
                        ttt.add(filterCOM.getItemAt(i));
                }
                allowAdd = false;
                filterUrlTF.setText("");
                filterCOM.removeAllItems();
                for (int i = 0 ; i< ttt.size(); ++i)
                    filterCOM.addItem(ttt.elementAt(i));
                allowAdd = true;
                filterCOM.updateUI();
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }
    @Override
    public void mouseReleased(MouseEvent e) {

    }
    @Override

    public void mouseEntered(MouseEvent e) {
        for (JButton b:panelBTNs)
            if(e.getSource().equals(b))
                b.setBorderPainted(true);
    }
    @Override
    public void mouseExited(MouseEvent e) {
        for (JButton b:panelBTNs)
            if(e.getSource().equals(b))
                b.setBorderPainted(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(allowAdd) {
            if (e.getSource().equals(filterCOM)) {
                String tmp = filterCOM.getSelectedItem().toString();
                filterUrlTF.setText(tmp);
            }
        }
    }
}

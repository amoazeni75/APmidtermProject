package GUI;

import Models.DownloadManagerBackEnd;
import Models.MainPanelTasks;
import Models.GUIParameters;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * this panel contains list of downloads
 */
public class DownloadListPanel extends JPanel implements MouseListener{
    private GUIParameters parameters;
    private Border emptyBorder;
    private Border selectBorder;
    private SpringLayout sp;
    private int lastSelectedPanel;
    private int currentSelectedPanel;
    private DownloadManagerBackEnd backEnd;

    public DownloadListPanel(GUIParameters para, DownloadManagerBackEnd backEnd){
        super();
        this.parameters = para;
        sp = new SpringLayout();
        setLayout(sp);
        this.backEnd = backEnd;
        setPreferredSize(new Dimension(parameters.getDownloadListWidth(),parameters.getDownloadListHeight()));
        setOpaque(false);
        currentSelectedPanel = -1;
        emptyBorder = BorderFactory.createEmptyBorder(1,1,1,1);
        selectBorder = BorderFactory.createLineBorder(Color.blue);
   }

    public void updateList(ArrayList<DownloadItem> objectList){
        this.removeAll();
        this.revalidate();
        this.repaint();
        if(objectList.size() <= 0)
            return;
        for (int i = 0; i < objectList.size(); ++i){
            add(objectList.get(i));
            if(i == 0)
                sp.putConstraint(SpringLayout.NORTH,objectList.get(i),1,SpringLayout.NORTH,this);
            else
                sp.putConstraint(SpringLayout.NORTH,objectList.get(i),1,SpringLayout.SOUTH,objectList.get(i-1));

            sp.putConstraint(SpringLayout.WEST,objectList.get(i),0,SpringLayout.WEST,this);
            objectList.get(i).addMouseListener(this);
        }
        int limit = (objectList.size() + 1) * 80;
        int currentSize = Math.max(8 * objectList.get(0).getSize().height,limit);
        if(currentSize > parameters.getMainPanelHeight()){
            int newHeight = currentSize + 85;
            this.setPreferredSize(new Dimension(parameters.getDownloadListWidth(),newHeight));
        }
    }

    public void setSelectedPanelBorder(int index){
        ArrayList<DownloadItem> objList = backEnd.getCurrentObjList();
        if(index != -1) {
            for (int i = 0; i < objList.size(); ++i) {
                if (i == index)
                    objList.get(i).setBorder(BorderFactory.createLineBorder(Color.red));
                else
                    objList.get(i).setBorder(BorderFactory.createEmptyBorder());
            }
        }
        else
        {
            for (int i = 0; i < objList.size(); ++i) {
                objList.get(i).setBorder(BorderFactory.createEmptyBorder());
            }
        }
    }

    public int getSelectedItemIndex(){
        return this.currentSelectedPanel;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        currentSelectedPanel = -1;
        ArrayList<DownloadItem> objList = backEnd.getCurrentObjList();
        if (SwingUtilities.isLeftMouseButton(e)) {
            int index = -1;
            boolean flag = false;
            for (DownloadItem di : objList) {
                if (e.getSource().equals(di)) {
                    index = objList.indexOf(di);
                    this.currentSelectedPanel = index;
                    flag = true;
                    break;
                }
            }
            if(flag)
                setSelectedPanelBorder(index);
            else
                currentSelectedPanel = -1;
        }
    }
    @Override
    public void mousePressed(MouseEvent e) {

    }
    @Override
    public void mouseReleased(MouseEvent e) {

    }
    @Override
    public void mouseEntered(MouseEvent e) {

    }
    @Override
    public void mouseExited(MouseEvent e) {

    }

    public int getCurrentSelectedPanel() {
        return currentSelectedPanel;
    }

    public void setCurrentSelectedPanel(int currentSelectedPanel) {
        this.currentSelectedPanel = currentSelectedPanel;
    }
}

package GUI;

import Models.DownloadItemBackend;
import Models.GUIParameters;

import javax.swing.*;
import java.awt.*;

/**
 * this frame shows every download item information
 */
public class DownloadInformationFrame extends JFrame {
    private JLabel urlIcon;
    private JLabel url;
    private JLabel contentIcon;
    private JLabel downloadName;
    private JLabel downloadSize;
    private JLabel location;
    private JLabel selectLocation;
    private JLabel sizeIcon;
    private JLabel timeIcon;
    private JLabel time;
    private JLabel startTimeLBL;
    private JLabel startTimeVal;
    private DownloadItemBackend backend;
    private SpringLayout sp;
    private GUIParameters parameters;

    public DownloadInformationFrame(GUIParameters para, DownloadItemBackend backend){
        super("Download Information");
        this.parameters = para;
        this.backend = backend;
        this.setPreferredSize(new Dimension(parameters.getDownloadInformationWidth(),(int)(0.8 *parameters.getDownloadInformationHeight())));
        sp = new SpringLayout();
        this.setLayout(sp);
        this.setResizable(false);
        setUpComponents();
        this.pack();
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }
    private void setUpComponents(){
        this.urlIcon = new JLabel(new ImageIcon("./icons/url.png"));
        this.url = new JLabel(backend.getMetadata().getUrl());
        this.contentIcon = new JLabel(new ImageIcon("./icons/filetype.png"));
        this.downloadName = new JLabel(backend.getMetadata().getFileName()+"."+backend.getMetadata().getFileType());
        this.downloadSize = new JLabel(DownloadItem.toMB(backend.getDownloadSize()));
        this.location = new JLabel(backend.getMetadata().getFilePath());
        this.selectLocation = new JLabel(new ImageIcon("./icons/selectLocation.png"));
        this.sizeIcon = new JLabel(new ImageIcon("./icons/fileSize.png"));
        this.timeIcon = new JLabel(new ImageIcon("./icons/time.png"));
        this.time = new JLabel(Integer.toString(backend.getDelayToStart()));
        this.startTimeLBL = new JLabel(new ImageIcon("./icons/calendar.png"));
        this.startTimeVal = new JLabel("Start Time : " +
                backend.getMetadata().getStartTime().toString() + "      "
        + "End Time : " + backend.getMetadata().getEndTime());

        addComponentsToPanel();
        setComponentSize();
        setComponentsLocation();
    }
    private void addComponentsToPanel(){
        add(urlIcon);
        add(url);
        add(contentIcon);
        add(downloadName);
        add(downloadSize);
        add(location);
        add(selectLocation);
        add(sizeIcon);
        add(timeIcon);
        add(time);
        add(startTimeLBL);
        add(startTimeVal);
    }
    private void setComponentSize(){
        int iconSize = 32;
        urlIcon.setPreferredSize(new Dimension(iconSize,iconSize));
        contentIcon.setPreferredSize(new Dimension(iconSize,iconSize));
        selectLocation.setPreferredSize(new Dimension(iconSize,iconSize));
        sizeIcon.setPreferredSize(new Dimension(iconSize,iconSize));
        timeIcon.setPreferredSize(new Dimension(iconSize,iconSize));
        startTimeLBL.setPreferredSize(new Dimension(iconSize,iconSize));
    }
    private void setComponentsLocation(){
        int horizontalGap = 20;
        int verticalGap = 10;
        sp.putConstraint(SpringLayout.NORTH,urlIcon,20,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,url,6,SpringLayout.NORTH,urlIcon);

        sp.putConstraint(SpringLayout.NORTH,contentIcon,verticalGap,SpringLayout.SOUTH,urlIcon);
        sp.putConstraint(SpringLayout.NORTH,downloadName,6,SpringLayout.NORTH,contentIcon);

        sp.putConstraint(SpringLayout.NORTH,sizeIcon,verticalGap,SpringLayout.SOUTH,contentIcon);
        sp.putConstraint(SpringLayout.NORTH,downloadSize,6,SpringLayout.NORTH,sizeIcon);

        sp.putConstraint(SpringLayout.NORTH,selectLocation,verticalGap,SpringLayout.SOUTH,sizeIcon);
        sp.putConstraint(SpringLayout.NORTH,location,6,SpringLayout.NORTH,selectLocation);

        sp.putConstraint(SpringLayout.NORTH,timeIcon,verticalGap,SpringLayout.SOUTH,selectLocation);
        sp.putConstraint(SpringLayout.NORTH,time,6,SpringLayout.NORTH,timeIcon);

        sp.putConstraint(SpringLayout.NORTH,startTimeLBL,verticalGap,SpringLayout.SOUTH,timeIcon);
        sp.putConstraint(SpringLayout.NORTH,startTimeVal,6,SpringLayout.NORTH,startTimeLBL);

        //west
        int tempGap = 10;
        sp.putConstraint(SpringLayout.WEST,urlIcon,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,url,verticalGap,SpringLayout.EAST,urlIcon);

        sp.putConstraint(SpringLayout.WEST,contentIcon,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,downloadName,verticalGap,SpringLayout.EAST,contentIcon);

        sp.putConstraint(SpringLayout.WEST,sizeIcon,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,downloadSize,verticalGap,SpringLayout.EAST,sizeIcon);

        sp.putConstraint(SpringLayout.WEST,selectLocation,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,location,verticalGap,SpringLayout.EAST,selectLocation);

        sp.putConstraint(SpringLayout.WEST,timeIcon,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,time,verticalGap,SpringLayout.EAST,timeIcon);

        sp.putConstraint(SpringLayout.WEST,startTimeLBL,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,startTimeVal,verticalGap,SpringLayout.EAST,timeIcon);

    }
}

package GUI;

import Models.*;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * new download frame
 */
public class NewDownloadWindow extends JFrame implements MouseListener {
    private JLabel urlIcon;
    private JTextField url;

    //private JLabel contentIcon;
    //private JTextField downloadName;
    //private JLabel downloadSize;

    private JTextField location;
    private JButton selectLocation;

    private JCheckBox openAfterDownloaded;
    private JLabel msgAfterDownload;

    private JLabel msgStartStatus;
    private JPanel startStatusButtons;
    private JRadioButton automatic;
    private JRadioButton manually;
    private JTextField startManuallyVal;
    private JRadioButton addToQueue;

    private JButton okBtn;
    private JButton cancelBtn;
    private SpringLayout sp;
    private GUIParameters parameters;
    private ArrayList<JButton> panelBtns;
    private ArrayList<JRadioButton> radioBtns;
    private DownloadManagerBackEnd backEnd;

    public NewDownloadWindow(GUIParameters para, DownloadManagerBackEnd backEnd){
        super("New Download");
        this.parameters = para;
        this.backEnd = backEnd;
        this.setPreferredSize(new Dimension(parameters.getNewWindowDownloadWidth(),parameters.getNewWindowDownloadHeight()));
        sp = new SpringLayout();
        this.setLayout(sp);
        this.setResizable(false);
        setUpComponents();
        this.pack();
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    private void setUpComponents(){
        this.urlIcon = new JLabel(new ImageIcon("./icons/url.png"));
        this.url = new JTextField();

        //this.contentIcon = new JLabel(new ImageIcon("./icons/filetype.png"));
        //this.downloadName = new JTextField();
        //this.downloadSize = new JLabel("0.0KB");

        this.location = new JTextField(ProgramSettings.getDefaultLocation());
        this.selectLocation = new JButton(new ImageIcon("./icons/selectLocation.png"));

        this.openAfterDownloaded = new JCheckBox();
        this.msgAfterDownload = new JLabel("Launch Download when it is Completed");

        this.msgStartStatus = new JLabel("Start with :");
        this.startStatusButtons = new JPanel(new GridLayout(3,1,3,3));
        this.automatic = new JRadioButton("Automatically");
        this.manually = new JRadioButton("Manually After ... Min");
        this.addToQueue = new JRadioButton ("Add to Queue");
        this.startManuallyVal = new JTextField();
        startManuallyVal.setEditable(false);

        okBtn = new JButton(new ImageIcon("./icons/ok.png"));
        cancelBtn = new JButton(new ImageIcon("./icons/cancel.png"));

        panelBtns = new ArrayList<>();
        radioBtns = new ArrayList<>();
        addComponentsToPanel();
        setComponentSize();
        setButtonsTransparent();
        setButtonListener();
        setButtonBoarders(false);
        setComponentsLocation();
    }
    private void addComponentsToPanel(){
        add(urlIcon);
        add(url);
        //add(contentIcon);
        //add(downloadName);
        //add(downloadSize);
        add(location);
        add(selectLocation);
        add(openAfterDownloaded);
        add(msgAfterDownload);
        add(msgStartStatus);
        add(startManuallyVal);
        add(startStatusButtons);
        add(cancelBtn);
        add(okBtn);
        startStatusButtons.add(automatic);
        startStatusButtons.add(manually);
        startStatusButtons.add(addToQueue);
        radioBtns.add(automatic);
        radioBtns.add(manually);
        radioBtns.add(addToQueue);

        panelBtns.add(selectLocation);
        panelBtns.add(okBtn);
        panelBtns.add(cancelBtn);
    }
    private void setComponentSize(){
        int iconSize = 32;
        int horizontalGap = (int)(0.1 * parameters.getNewWindowDownloadWidth());
        int iconTextGap = 10;
        int textFieldWidth = (parameters.getNewWindowDownloadWidth() - iconSize - 2 * horizontalGap - iconTextGap );
        urlIcon.setPreferredSize(new Dimension(iconSize,iconSize));
        url.setPreferredSize(new Dimension(textFieldWidth,url.getPreferredSize().height));

        //contentIcon.setPreferredSize(new Dimension(iconSize,iconSize));
        //downloadName.setPreferredSize(new Dimension(textFieldWidth,downloadName.getPreferredSize().height));

        location.setPreferredSize(new Dimension(textFieldWidth,location.getPreferredSize().height));
        selectLocation.setPreferredSize(new Dimension(iconSize,iconSize));

        startManuallyVal.setPreferredSize(new Dimension(50,startManuallyVal.getPreferredSize().height));

        okBtn.setPreferredSize(new Dimension(2*iconSize,2*iconSize));
        cancelBtn.setPreferredSize(new Dimension(2*iconSize,2*iconSize));
    }
    private void setButtonsTransparent(){
        selectLocation.setContentAreaFilled(false);
        automatic.setContentAreaFilled(false);
        manually.setContentAreaFilled(false);
        addToQueue.setContentAreaFilled(false);
        okBtn.setContentAreaFilled(false);
        cancelBtn.setContentAreaFilled(false);
        startStatusButtons.setOpaque(false);
    }
    private void setButtonListener(){
        selectLocation.addMouseListener(this);
        okBtn.addMouseListener(this);
        cancelBtn.addMouseListener(this);
        automatic.addMouseListener(this);
        manually.addMouseListener(this);
        addToQueue.addMouseListener(this);
     }
    private void setButtonBoarders(boolean state){
        cancelBtn.setBorderPainted(state);
        okBtn.setBorderPainted(state);
        selectLocation.setBorderPainted(state);
    }
    private void setComponentsLocation(){
        int horizontalGap = 20;
        int verticalGap = 10;
        sp.putConstraint(SpringLayout.NORTH,urlIcon,20,SpringLayout.NORTH,this);
        sp.putConstraint(SpringLayout.NORTH,url,6,SpringLayout.NORTH,urlIcon);
        //sp.putConstraint(SpringLayout.NORTH,contentIcon,verticalGap,SpringLayout.SOUTH,urlIcon);
        //sp.putConstraint(SpringLayout.NORTH,downloadName,6,SpringLayout.NORTH,contentIcon);
        //sp.putConstraint(SpringLayout.NORTH,downloadSize,verticalGap,SpringLayout.SOUTH,downloadName);

        sp.putConstraint(SpringLayout.NORTH,selectLocation,4,SpringLayout.SOUTH,urlIcon);
        sp.putConstraint(SpringLayout.NORTH,location,verticalGap,SpringLayout.SOUTH,urlIcon);

        sp.putConstraint(SpringLayout.NORTH,openAfterDownloaded,2 * verticalGap,SpringLayout.SOUTH,location);
        sp.putConstraint(SpringLayout.NORTH,msgAfterDownload,2 * verticalGap,SpringLayout.SOUTH,selectLocation);
        sp.putConstraint(SpringLayout.NORTH,msgStartStatus,verticalGap,SpringLayout.SOUTH,msgAfterDownload);
        sp.putConstraint(SpringLayout.NORTH,startStatusButtons,verticalGap,SpringLayout.SOUTH,msgStartStatus);
        sp.putConstraint(SpringLayout.NORTH,startManuallyVal,3 * verticalGap,SpringLayout.NORTH,startStatusButtons);
        sp.putConstraint(SpringLayout.SOUTH,okBtn,-50,SpringLayout.SOUTH,this);
        sp.putConstraint(SpringLayout.NORTH,okBtn,-64,SpringLayout.SOUTH,okBtn);
        sp.putConstraint(SpringLayout.SOUTH,cancelBtn,-50,SpringLayout.SOUTH,this);
        sp.putConstraint(SpringLayout.NORTH,cancelBtn,-64,SpringLayout.SOUTH,cancelBtn);

        //west
        int tempGap = 10;
        sp.putConstraint(SpringLayout.WEST,urlIcon,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,url,tempGap,SpringLayout.EAST,urlIcon);
        //sp.putConstraint(SpringLayout.WEST,contentIcon,horizontalGap,SpringLayout.WEST,this);
        //sp.putConstraint(SpringLayout.WEST,downloadName,tempGap,SpringLayout.EAST,contentIcon);
        //sp.putConstraint(SpringLayout.WEST,downloadSize,tempGap,SpringLayout.EAST,contentIcon);

        sp.putConstraint(SpringLayout.WEST,selectLocation,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,location,tempGap,SpringLayout.EAST,selectLocation);

        sp.putConstraint(SpringLayout.WEST,openAfterDownloaded,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,msgAfterDownload,tempGap,SpringLayout.EAST,openAfterDownloaded);
        sp.putConstraint(SpringLayout.WEST,msgStartStatus,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,startStatusButtons,horizontalGap,SpringLayout.WEST,this);
        sp.putConstraint(SpringLayout.WEST,startManuallyVal,horizontalGap,SpringLayout.EAST,startStatusButtons);
        sp.putConstraint(SpringLayout.EAST,cancelBtn,-50,SpringLayout.EAST,this);
        sp.putConstraint(SpringLayout.WEST,cancelBtn,-64,SpringLayout.EAST,cancelBtn);
        sp.putConstraint(SpringLayout.EAST,okBtn,-20,SpringLayout.WEST,cancelBtn);
        sp.putConstraint(SpringLayout.WEST,okBtn,-64,SpringLayout.EAST,okBtn);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(SwingUtilities.isLeftMouseButton(e)) {
            if (e.getSource().equals(selectLocation)) {
                JFileChooser fc = new JFileChooser();
                fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                    location.setText(fc.getSelectedFile().getAbsolutePath());
                }
            }
            else if (radioBtns.contains(e.getSource())) {
                int index = radioBtns.indexOf(e.getSource());
                radioBtns.get(index).setSelected(true);
                if (e.getSource().equals(manually))
                    startManuallyVal.setEditable(true);
                else
                    startManuallyVal.setEditable(false);
                for (int i = 0; i < 3; i++) {
                    if (i == index)
                        continue;
                    else
                        radioBtns.get(i).setSelected(false);
                }
            }
            else if(e.getSource().equals(cancelBtn))
                this.dispose();
            else if(e.getSource().equals(okBtn)){
                boolean urlAccepted = true;
                urlAccepted = backEnd.acceptUrl(url.getText());
                if(!urlAccepted){
                    final JPanel panel = new JPanel();
                    JOptionPane.showMessageDialog(panel, "this url is filtered", "Error", JOptionPane.ERROR_MESSAGE);
                    url.setText("");
                    //downloadName.setText("");
                }
                else {

                    int delay = 0;
                    if (manually.isSelected()) {
                        String delayVal = startManuallyVal.getText();

                        try {
                            delay = Integer.parseInt(delayVal);
                        } catch (NumberFormatException h) {
                            delay = 0;
                        }
                    }
                    if(url.getText().equals(""))
                        return;
                    backEnd.addNewDownload(url.getText(), location.getText(),
                            openAfterDownloaded.isSelected(), delay, addToQueue.isSelected(),automatic.isSelected());
                    this.dispose();
                }

            }
        }

    }
    @Override
    public void mousePressed(MouseEvent e) {

    }
    @Override
    public void mouseReleased(MouseEvent e) {

    }
    @Override
    public void mouseEntered(MouseEvent e) {
        for (JButton b:panelBtns)
            if(e.getSource().equals(b))
                b.setBorderPainted(true);
    }
    @Override
    public void mouseExited(MouseEvent e) {
        for (JButton b:panelBtns)
            if(e.getSource().equals(b))
                b.setBorderPainted(false);
    }

}

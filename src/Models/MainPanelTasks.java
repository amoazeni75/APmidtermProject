package Models;

import GUI.DownloadItem;
import java.io.IOException;

/**
 * this interface define some action on mainTask panel
 */
public interface MainPanelTasks {
    void addNewDownload();
    void updateDownloadListGUI(int index);
    void resumeDownload();
    void resumeDownload(DownloadItem e);
    void removeDownload();
    void removeDownload(DownloadItem e);
    void cancelDownload();
    void cancelDownload(DownloadItem e);
    void pauseDownload();
    void setting();
    void moveUpInList(DownloadItem e);
    void moveDownInLis(DownloadItem e);
    void addToQueue(DownloadItem e);
    void restore(DownloadItem e);
    void search(String in);
    void sort(int mode);
    void sortOrder(int mode);
    void export() throws IOException;
}

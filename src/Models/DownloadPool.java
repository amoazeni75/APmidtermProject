package Models;

import GUI.DownloadItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class DownloadPool extends Thread implements Observer {
    private static DownloadPool pool;
    private List<DownloadItem> downloadManagers;
    private List<Thread> 	   downloadThreads;
    private List<DownloadItem> removedManagers;


    /**
     * Only single instance of the class is possible through <code>pool</code> object.
     */
    public DownloadPool(int poolCount) {
        downloadManagers = new ArrayList<>();
        removedManagers  = new ArrayList<>();
        downloadThreads  = new ArrayList<>();
    }

    /**
     * Get the reference of download pool.
     * @return
     */
    public static DownloadPool getDownloadPool(int poolCount) {
        if(pool == null)
            pool = new DownloadPool(poolCount);

        return pool;
    }

    /**
     * Gets, a download manager if exists in the download pool.
     * @param downloadId
     * @return Returns reference of DownloadManager if exists in download pool, otherwise null.
     */
    public DownloadItem get(long downloadId) {
        for(DownloadItem manager : downloadManagers) {
            if(manager.getBackend().getMetadata().getId() == downloadId)
                return manager;
        }
        return null;
    }

    /**
     * Add a DownloadManager to the download pool. Calling this automatically starts the DownloadManager.
     * @param manager
     */
    public synchronized void add(DownloadItem manager) {
        downloadManagers.add(manager);
        manager.getBackend().addObserver(this);
        start(manager.getBackend());
    }

    /**
     * Starts the DownloadManager on a new download Thread.
     * @param manager
     */
    private void start(DownloadItemBackend manager) {
        if(manager != null) {
            Thread t = new Thread(manager);
            t.setName(manager.getDownloadId() + "");
            downloadThreads.add(t);
            t.start();
        }
    }


    /**
     * Removes the DownloadManager from the download pool. Calling this will automatically pause the active download.
     * Calling this is similar to calling <code>remove(manager, true)</code>
     * @param manager
     */
    public synchronized void remove(DownloadItem manager) {
        remove(manager, true);
    }


    /**
     * Removes the DownloadManager from the download pool.
     * @param manager
     * @param stop true if you want to pause the download if in progress, otherwise false.
     */
    public synchronized void remove(DownloadItem manager, boolean stop) {
        if(manager != null && downloadManagers.contains(manager)) {
            if(stop)
                manager.getBackend().pause();
            else {
                removedManagers.add(manager);
                downloadManagers.remove(manager);
            }
        }
    }

    /**
     * Removes the DownloadManager from the download pool. Calling this will automatically pause the active download.
     * Calling this is similar to calling <code>remove(manager, true)</code>
     * @param downloadId
     */
    public synchronized void remove(long downloadId) {
        DownloadItem manager = get(downloadId);
        remove(manager);
    }

    /**
     * Pauses all active downloads and removes from the download pool.
     */
    public synchronized void removeAll() {
        List<Long> ids = new ArrayList<>();

        downloadManagers.forEach((manager)->{
            ids.add(manager.getBackend().getDownloadId());
        });

        ids.forEach((id)-> {
            remove(id);
        });
    }


    @SuppressWarnings("unused")
    private Thread getDownloadThread(long downloadId) {
        for(Thread t : downloadThreads) {
            if(t.getName().equals(downloadId + ""))
                return t;
        }

        return null;
    }

    /**
     * @return Returns total active downloads at the current moment.
     */
    public int activeDownloadCount() {
        return downloadManagers.size();
    }


    /**
     * Removes the reference of a download manager if it completed download.
     */
    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof DownloadItemBackend) {
            DownloadItemBackend manager = (DownloadItemBackend) o;

            DownloadStatus status = manager.getStatus();
            if(status == DownloadStatus.PAUSED || status == DownloadStatus.ERROR) {
                // If current download is paused or some error occurred
                // Then remove it from download pool

                remove(manager.getMainBackEnd().getGUI(manager), false);
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        removeAll();
        super.finalize();
    }
    public boolean containThisDownload(DownloadItem e){
        return downloadManagers.contains(e);
    }
    public int activeDownload(){
        return downloadManagers.size();
    }
}

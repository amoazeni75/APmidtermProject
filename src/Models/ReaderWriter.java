package Models;

import java.io.*;
import java.util.ArrayList;

/**
 * Autor : S.Alireza Moazeni 9423110
 * this class do reading and writing </br>
 * this class do serialize and non serialize action
 */
public class ReaderWriter {
    public static <T> void fileWriter(T dataWrapper,String path){
        try(FileOutputStream fs = new FileOutputStream(path)){
            ObjectOutputStream os  = new ObjectOutputStream(fs);
            os.writeObject(dataWrapper);
        } catch (FileNotFoundException e) {
            System.out.println("File Not Fount !!!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T> T fileReader(String path){
        try(FileInputStream fs = new FileInputStream(path)){
            ObjectInputStream os  = new ObjectInputStream(fs);
            return (T)os.readObject();
        } catch (FileNotFoundException e) {
            System.out.println("File Not Fount !!!");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void fileWriter(ArrayList<String> input, String path) {
        try {
            File file = new File(path);
            FileWriter fileWriter = new FileWriter(file);
            for (int i = 0; i < input.size() - 1; i+= 2) {
             fileWriter.write(input.get(i) + "\n");
             fileWriter.write(input.get(i + 1)+"\n");
             fileWriter.write("\n");
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void fileWriterCommon(ArrayList<String> input, String path) {
        try {
            File file = new File(path);
            FileWriter fileWriter = new FileWriter(file);
            for (int i = 0; i < input.size(); i++) {
                fileWriter.write(input.get(i) + "\n");
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static ArrayList<String> fileReaderCommon(String path){
        ArrayList<String> res = new ArrayList<>();

            try (BufferedReader br = new BufferedReader(new FileReader(new File(path)))) {
                String line;
                while ((line = br.readLine()) != null) {
                    res.add(line);
                }
            } catch (IOException e) {
                System.out.println("file not found!!!");
            }
        return res;
    }
}

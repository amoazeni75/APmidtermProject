package Models;

import java.awt.*;

/**
 * this class contains some parameters like panel's size </br>
 * also frame and button size
 */
public class GUIParameters {
    private final int mainFrameWidth;
    private final int mainFrameHeight;
    private final int mainPanelWidth;
    private final int mainPanelHeight;
    private final int cToolBarWidth;
    private final int cTollBarHeight;
    private final int newWindowDownloadWidth;
    private final int newWindowDownloadHeight;
    private final int leftPanelWidth;
    private final int leftPanelHeight;
    private final int downloadListWidth;
    private final int downloadListHeight;
    private final int downloadItemWidth;
    private final int downloadItemHeight;
    private final int settingFrameWidth;
    private final int settingFrameHeight;
    private final int downloadInformationWidth;
    private final int downloadInformationHeight;

    public int getMainFrameWidth() {
        return mainFrameWidth;
    }
    public int getMainFrameHeight() {
        return mainFrameHeight;
    }
    public int getMainPanelWidth() {
        return mainPanelWidth;
    }
    public int getMainPanelHeight() {
        return mainPanelHeight;
    }
    public int getcToolBarWidth() {
        return cToolBarWidth;
    }
    public int getcTollBarHeight() {
        return cTollBarHeight;
    }
    public int getNewWindowDownloadWidth() {
        return newWindowDownloadWidth;
    }
    public int getNewWindowDownloadHeight() {
        return newWindowDownloadHeight;
    }
    public int getLeftPanelWidth() {
        return leftPanelWidth;
    }
    public int getLeftPanelHeight() {
        return leftPanelHeight;
    }
    public int getDownloadListWidth() {
        return downloadListWidth;
    }
    public int getDownloadListHeight() {
        return downloadListHeight;
    }

    public int getDownloadItemWidth() {
        return downloadItemWidth;
    }

    public int getDownloadItemHeight() {
        return downloadItemHeight;
    }

    public int getSettingFrameWidth() {
        return settingFrameWidth;
    }

    public int getSettingFrameHeight() {
        return settingFrameHeight;
    }

    public GUIParameters(){
        mainFrameWidth = (int)(Toolkit.getDefaultToolkit().getScreenSize().width * 0.60);
        mainFrameHeight = (int)(Toolkit.getDefaultToolkit().getScreenSize().height * 0.75);
        mainPanelWidth = (int)(mainFrameWidth * 0.78);
        mainPanelHeight = mainFrameHeight;
        cToolBarWidth = mainPanelWidth;
        cTollBarHeight = (int)(mainPanelHeight*0.07);
        newWindowDownloadWidth = (int)(0.6 * mainFrameWidth);
        newWindowDownloadHeight = (int)(0.45 * mainFrameHeight);
        leftPanelWidth = (int)(mainFrameWidth * 0.2);
        leftPanelHeight = mainFrameHeight;
        downloadListWidth = mainPanelWidth;
        downloadListHeight = (int)(mainPanelHeight * 0.93);
        downloadItemWidth = downloadListWidth - 15;
        downloadItemHeight = 85;
        settingFrameWidth = (int)(0.65 * mainFrameWidth);
        settingFrameHeight = (int)(0.63 * mainFrameHeight);
        downloadInformationWidth = (int)(0.6 * mainFrameWidth);
        downloadInformationHeight = (int)(0.50 * mainFrameHeight);

    }

    public int getDownloadInformationWidth() {
        return downloadInformationWidth;
    }

    public int getDownloadInformationHeight() {
        return downloadInformationHeight;
    }
}

package Models;

import GUI.DownloadItem;
import GUI.MainFrame;

import javax.swing.*;
import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * this is the main backend of programme
 */
public class DownloadManagerBackEnd {

    private ArrayList<DownloadItem> processingDownloads;
    private ArrayList<DownloadItem> precessingDownloading; // state = downloading
    private ArrayList<DownloadItem> processingWaiting; //state = in queue and ready for download

    private ArrayList<DownloadItem> queueDownloads;
    private ArrayList<DownloadItem> queueDownloading;
    private ArrayList<DownloadItem> queueWaiting;

    private int processingSortOrder;
    private int queueSortOrder;
    private ArrayList<DownloadItem> completedDownloads;
    private int completedSortOrder;
    private ArrayList<DownloadItem> removedDownloads;
    private int removedSortOrder;
    private ArrayList<DownloadItem> currentDownloadList;
    private ArrayList<DownloadItem> searchResult;
    private int searchSortOrder;
    private ArrayList<String> fullRemoved;
    private ArrayList<String> filterUrl;
    private int currentListId;
    private GUIParameters guiParameters;
    private ProgramSettings programSettings;
    private MainFrame mainFrame;
    private DownloadPool pool = DownloadPool.getDownloadPool(ProgramSettings.parallelDownloadCount);
    private DownloadPool queuePool = new DownloadPool(1);


    public DownloadManagerBackEnd(GUIParameters guiParameters, MainFrame mainFrame) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        processingDownloads = new ArrayList<>();
        precessingDownloading = new ArrayList<>();
        processingWaiting = new ArrayList<>();

        queueDownloads = new ArrayList<>();
        queueDownloading = new ArrayList<>();
        queueWaiting = new ArrayList<>();

        completedDownloads = new ArrayList<>();
        searchResult = new ArrayList<>();
        removedDownloads = new ArrayList<>();
        fullRemoved = new ArrayList<>();
        filterUrl = new ArrayList<>();

        currentDownloadList = processingDownloads;
        currentListId = 0;
        this.guiParameters = guiParameters;
        this.mainFrame = mainFrame;

        ArrayList<String> tmpSettings = ReaderWriter.fileReaderCommon("./ApplicationData/Settings.jdm");
        programSettings = new ProgramSettings();
        if (tmpSettings.size() > 0) {
            int threadCount = 200;
            int conncectionCount = 1;
            try {
                threadCount = Integer.parseInt(tmpSettings.get(0));
                conncectionCount = Integer.parseInt(tmpSettings.get(1));
            } catch (NumberFormatException eo) {
            }
            programSettings.setParallelDownloadCount(threadCount);
            ProgramSettings.connectionCountPerDownload = conncectionCount;
            programSettings.setDefaultLocation(tmpSettings.get(2));
            programSettings.setLookAndFeel(tmpSettings.get(3));
        }
        processingSortOrder = 0;
        queueSortOrder = 0;
        completedSortOrder = 0;
        removedSortOrder = 0;
        searchSortOrder = 0;
    }

    /**
     * this method implement action of zip files
     * @param fileName
     * @param zos
     * @throws IOException
     */
    public static void addToZipFile(String fileName, ZipOutputStream zos) throws IOException {

        System.out.println("Writing '" + fileName + "' to zip file");

        File file = new File(fileName);
        FileInputStream fis = new FileInputStream(file);
        ZipEntry zipEntry = new ZipEntry(fileName);
        zos.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }
        zos.closeEntry();
        fis.close();
    }

    /**
     * adding new download
     * @param URL
     * @param Location
     * @param openAfterDownload
     * @param delay
     * @param addToQueue
     * @param startEmidiatly
     */
    public void addNewDownload(String URL, String Location, boolean openAfterDownload, int delay, boolean addToQueue, boolean startEmidiatly) {

        DownloadItemBackend dItem = new DownloadItemBackend(URL, Location + "\\");

        dItem.setLaunchAfterDownload(openAfterDownload);
        dItem.setMainBackEnd(this);
        dItem.setFetchFirstData(true);
        dItem.setDownloadLunchStatus(0);
        if (delay > 0) {
            dItem.setDelayToStart(delay);
            dItem.setDownloadLunchStatus(1);
        }
        if (addToQueue)
            dItem.setDownloadLunchStatus(2);

        DownloadItem dItemGUI = new DownloadItem(dItem, guiParameters, this);
        dItemGUI.getBackend().addObserver(dItemGUI);
        dItem.setGui(dItemGUI);
        if (addToQueue) {
            queueDownloads.add(queueDownloads.size(), dItemGUI);
            dItemGUI.setDequeueIcon();
            dItem.setDownloadListId(1);
            currentListId = 1;
            queuePool.add(dItemGUI);//get initial information
            if (queueDownloading.size() == 0 && startEmidiatly) {
                queueDownloading.add(dItemGUI);
                queuePool.add(dItemGUI);
            }
            else
                queueWaiting.add(dItemGUI);
        }
        else { //add to processing download
            processingDownloads.add(dItemGUI);
            currentListId = 0;
            dItem.setDownloadListId(0);
            pool.add(dItemGUI); //get initial information
            if (precessingDownloading.size() < ProgramSettings.parallelDownloadCount && startEmidiatly) {
                precessingDownloading.add(dItemGUI);
                pool.add(dItemGUI);
            } else
                processingWaiting.add(dItemGUI);
        }
        updateDownloadListGUI();
        mainFrame.getlPanel().setSelectedPanelBorder(currentListId);
    }

    /**
     * resume selected download item
     * @param index
     */
    public void resumeDownload(int index) {
        if (currentListId == 2 || currentListId == 3)
            return;
        if (index == -1 && currentListId != 1)
            return;
        if (currentListId == 1) {
            if (queueDownloading.size() == 0) {
                if (queueWaiting.size() > 0) {
                    DownloadItem target = queueWaiting.get(0);
                    queueDownloading.add(target);
                    queueWaiting.remove(target);
                    queuePool.add(target);
                }
            }
        }
        else if (currentListId == 0 && index != -1) {
            DownloadItem target = currentDownloadList.get(index);
            //first detect that this download is downloading or waiting
            if (precessingDownloading.contains(target))
                return;
            else if (precessingDownloading.size() < ProgramSettings.parallelDownloadCount) {
                precessingDownloading.add(target);
                processingWaiting.remove(target);
                pool.add(target);
            }
        }
    }

    public void resumeDownload(DownloadItem e) {
        resumeDownload(currentDownloadList.indexOf(e));
    }

    /**
     * remove selected download
     * @param index
     */
    public void removeDownload(int index) {
        if (index != -1 && currentDownloadList.size() > 0 && index < currentDownloadList.size()) {
            DownloadItem target = currentDownloadList.get(index);

            target.getBackend().setListIdBeforeDelete(currentListId);
            target.getBackend().setDownloadListId(3);

            if (currentListId == 0) {
                if (precessingDownloading.contains(target)) {
                    precessingDownloading.remove(target);
                    pool.remove(target);
                }
                else processingWaiting.remove(target);
                removedDownloads.add(target);
            }
            else if (currentListId == 1) {
                if(queueDownloading.contains(target)){
                    queueDownloading.remove(target);
                    queuePool.remove(target);
                }
                else queueWaiting.remove(target);
                removedDownloads.add(target);
            }
            else if (currentListId == 2) {
                removedDownloads.add(target);
            }
            else if (currentListId == 3) {
                fullRemoved.add(target.getBackend().getMetadata().getFileName());
                fullRemoved.add(target.getBackend().getMetadata().getUrl());
            }
            this.currentDownloadList.remove(index);
            updateDownloadListGUI();
        }
    }

    public void removeDownload(DownloadItem e) {
        removeDownload(currentDownloadList.indexOf(e));
    }

    /**
     * cancel selected download item
     * @param index
     */
    public void cancelDownload(int index) {
        if (currentListId == 3 || currentListId == 2 || index == -1)
            return;
        if (currentListId == 1 && index != -1) {
            DownloadItem target = currentDownloadList.get(index);
            if (queueDownloading.contains(target)) {
                queueDownloading.remove(target);
                queueWaiting.add(target);
                queuePool.remove(target);
            }
        }
        else if (currentListId == 0 && index != -1) {
            DownloadItem target = currentDownloadList.get(index);
            if (precessingDownloading.contains(target)) {
                precessingDownloading.remove(target);
                processingWaiting.add(target);
                pool.remove(target);
            }
        }
    }

    public void cancelDownload(DownloadItem e) {
        cancelDownload(currentDownloadList.indexOf(e));
    }

    /**
     * pause selected download item
     * @param index
     */
    public void pauseDownload(int index) {
        if (currentListId == 3 || currentListId == 2)
            return;
        if (currentListId == 1) {
            if (queueDownloading.size() > 0) {
                DownloadItem target = queueDownloading.get(0);
                queueDownloading.remove(target);
                queueWaiting.add(0,target);
                target.getBackend().pause();
            }
        }
        else if (currentListId == 0 && index != -1) {
            DownloadItem target = currentDownloadList.get(index);
            if (precessingDownloading.contains(target)) {
                precessingDownloading.remove(target);
                processingWaiting.add(target);
                target.getBackend().pause();
            }
        }
    }

    /**
     * moving item in list to up
     * @param e
     */
    public void moveUpInList(DownloadItem e) {
        if(currentListId == 1 && queueDownloading.size() != 0)
            return;
        int currentIndex = currentDownloadList.indexOf(e);
        int afterChange = currentIndex - 1;
        if (afterChange >= 0) {
            DownloadItem tmp = currentDownloadList.get(currentIndex);
            currentDownloadList.set(currentIndex, currentDownloadList.get(afterChange));
            currentDownloadList.set(afterChange, tmp);
        }
        if(currentListId == 1){
            queueDownloading.clear();
            queueWaiting.clear();
            for (int i = 0; i < currentDownloadList.size(); i++) {
                queueWaiting.add(currentDownloadList.get(i));
            }
        }
        updateDownloadListGUI();

    }

    /**
     * move selected item in list down
     * @param e
     */
    public void moveDownInLis(DownloadItem e) {
        if(currentListId == 1 && queueDownloading.size() != 0)
            return;
        int currentIndex = currentDownloadList.indexOf(e);
        int afterChange = currentIndex + 1;
        if (afterChange <= currentDownloadList.size() - 1) {
            DownloadItem tmp = currentDownloadList.get(currentIndex);
            currentDownloadList.set(currentIndex, currentDownloadList.get(afterChange));
            currentDownloadList.set(afterChange, tmp);
        }
        if(currentListId == 1){
            queueDownloading.clear();
            queueWaiting.clear();
            for (int i = 0; i < currentDownloadList.size(); i++) {
                queueWaiting.add(currentDownloadList.get(i));
            }
        }
        updateDownloadListGUI();
    }

    /**
     * adding new item to queue
     * @param e
     */
    public void addToQueue(DownloadItem e) {
        currentDownloadList.remove(e);
        if (currentDownloadList == processingDownloads) {
            //first we stop download if its state is downloading
            if (precessingDownloading.contains(e)) {
                precessingDownloading.remove(e);
                pool.remove(e);
            }
            else
                processingWaiting.remove(e);

            queueDownloads.add(e);
            e.getBackend().setDownloadListId(1);
        }
        else if (currentDownloadList == queueDownloads) {
            //first we stop it if its state is downloading
            if (queueDownloading.contains(e)) {
                queueDownloading.remove(e);
                queuePool.remove(e);
            }
            else
                queueWaiting.remove(e);
            processingDownloads.add(e);
            e.getBackend().setDownloadListId(0);
        }
        updateDownloadListGUI();
    }

    public ArrayList<DownloadItem> getCurrentObjList() {
        return currentDownloadList;
    }

    /**
     *    restore seleced remove item
      */

    public void restore(DownloadItem e) {
        if (currentListId == 3) {
            removedDownloads.remove(e);
            if (e.getBackend().getListIdBeforeDelete() == 0) {
                processingDownloads.add(e);
                processingWaiting.add(e);
            }
            else if (e.getBackend().getListIdBeforeDelete() == 1) {
                queueDownloads.add(e);
                queueWaiting.add(e);
            }

            else if (e.getBackend().getListIdBeforeDelete() == 2)
                completedDownloads.add(e);

            e.getBackend().setDownloadListId(e.getBackend().getListIdBeforeDelete());
            currentListId = e.getBackend().getListIdBeforeDelete();
            mainFrame.getlPanel().setSelectedPanelBorder(e.getBackend().getListIdBeforeDelete());
            updateDownloadListGUI();
        }
    }

    /**
     * load one list from file
     * @param dItems
     * @param path
     */
    public void loadOneList(ArrayList<DownloadItem> dItems, String path) {
        ArrayList<BufferData> bufferData;
        bufferData = ReaderWriter.fileReader(path);
        if (bufferData != null) {
            for (int i = 0; i < bufferData.size(); i++) {
                long iddd = Long.parseLong(bufferData.get(i).getId());
                boolean ran = false;
                Date startDate = new Date();
                startDate.setTime(Long.parseLong(bufferData.get(i).getStartTime()));
                Date endDate = new Date();
                endDate.setTime(Long.parseLong(bufferData.get(i).getEndTime()));
                if (bufferData.get(i).getRangeAllowed().equals("true"))
                    ran = true;
                DownloadStatus ds = null;
                switch (bufferData.get(i).getStatus()) {
                    case "NEW":
                        ds = DownloadStatus.NEW;
                    case "READY":
                        ds = DownloadStatus.READY;
                    case "DOWNLOADING":
                        ds = DownloadStatus.DOWNLOADING;
                    case "COMPLETED":
                        ds = DownloadStatus.COMPLETED;
                    case "PAUSED":
                        ds = DownloadStatus.PAUSED;
                    case "ERROR":
                        ds = DownloadStatus.ERROR;
                }
                long downloadCompleted = Long.parseLong(bufferData.get(i).getCompleted());
                long downloadSize = Long.parseLong(bufferData.get(i).getSize());
                DownloadMetaData meta = new DownloadMetaData(iddd, bufferData.get(i).getUrl(), startDate, endDate
                        , bufferData.get(i).getPath(), ds, downloadCompleted, bufferData.get(i).getName(),
                        bufferData.get(i).getType(), downloadSize, ran);
                List<DownloadPartsMetadata> parts = bufferData.get(i).getParts();

                DownloadItemBackend tmpBack = new DownloadItemBackend(meta, parts);

                tmpBack.setMainBackEnd(this);
                DownloadItem dItemGUI = new DownloadItem(tmpBack, guiParameters, this);
                dItemGUI.getBackend().addObserver(dItemGUI);
                tmpBack.setGui(dItemGUI);
                dItemGUI.setValsAfterLoad();
                dItems.add(dItemGUI);
            }
        }
    }

    public void loadAll() {
        loadOneList(processingDownloads, ProgramSettings.processingPath);
        loadOneList(queueDownloads, ProgramSettings.queuePath);
        for (int i = 0; i < processingDownloads.size(); i++) {
            processingWaiting.add(processingDownloads.get(i));
        }
        for (int i = 0; i < queueDownloads.size(); i++) {
            queueDownloads.get(i).setDequeueIcon();
            queueDownloads.get(i).getBackend().setDownloadListId(1);
            queueWaiting.add(queueDownloads.get(i));
        }
        loadOneList(removedDownloads, ProgramSettings.removePath);
        loadOneList(completedDownloads, ProgramSettings.completedPath);

        ArrayList<String> tmpUrl = ReaderWriter.fileReaderCommon(ProgramSettings.filterUrlPath);
        this.filterUrl = tmpUrl;
        currentListId = 0;
        //updateDownloadListGUI();
    }

    public void saveOneList(ArrayList<DownloadItem> dItems, String path) {
        ArrayList<BufferData> bufferData;
        bufferData = new ArrayList<>();
        for (int i = 0; i < dItems.size(); ++i) {
            BufferData bf = new BufferData();
            bf.setCompleted(Long.toString(dItems.get(i).getBackend().getMetadata().getCompleted()));
            bf.setStartTime(Long.toString(dItems.get(i).getBackend().getMetadata().getStartTime().getTime()));
            bf.setName(dItems.get(i).getBackend().getMetadata().getFileName());
            bf.setStatus(dItems.get(i).getBackend().getMetadata().getStatus().toString());
            bf.setEndTime(Long.toString(dItems.get(i).getBackend().getMetadata().getEndTime().getTime()));
            bf.setUrl(dItems.get(i).getBackend().getMetadata().getUrl());
            bf.setPath(dItems.get(i).getBackend().getMetadata().getFilePath());
            bf.setType(dItems.get(i).getBackend().getMetadata().getFileType());
            bf.setSize(Long.toString(dItems.get(i).getBackend().getMetadata().getFileSize()));
            bf.setDownloadListId(Integer.toString(dItems.get(i).getBackend().getDownloadListId()));
            bf.setId(Long.toString(dItems.get(i).getBackend().getMetadata().getId()));
            if (dItems.get(i).getBackend().getMetadata().isRangeAllowed())
                bf.setRangeAllowed("true");
            else
                bf.setRangeAllowed("false");
            bufferData.add(bf);
            for (int j = 0; j < dItems.get(i).getBackend().getPartsMetaList().size(); j++) {
                bf.addPart(new BufferPart(dItems.get(i).getBackend().getPartsMetaList().get(j).getDownloadId(),
                        dItems.get(i).getBackend().getPartsMetaList().get(j).getId(),
                        dItems.get(i).getBackend().getPartsMetaList().get(j).getStart(),
                        dItems.get(i).getBackend().getPartsMetaList().get(j).getEnd(),
                        dItems.get(i).getBackend().getPartsMetaList().get(j).getPath()));
            }
        }
        ReaderWriter.fileWriter(bufferData, path);

    }

    public ArrayList<String> getFilterUrl() {
        return filterUrl;
    }

    public void setFilterUrl(ArrayList<String> filterUrl) {
        this.filterUrl = filterUrl;
    }

    /**
     * save all settings and download item and queue into file
     */
    public void saveAll() {

        saveOneList(processingDownloads, ProgramSettings.processingPath);
        saveOneList(queueDownloads, ProgramSettings.queuePath);
        saveOneList(removedDownloads, ProgramSettings.removePath);
        saveOneList(completedDownloads, ProgramSettings.completedPath);

        ReaderWriter.fileWriterCommon(filterUrl, ProgramSettings.filterUrlPath);

        ReaderWriter.fileWriter(fullRemoved, ProgramSettings.finalRemovePath);

        ArrayList<String> tmpSettings = new ArrayList<>();
        tmpSettings.add(Integer.toString(programSettings.getParallelDownloadCount()));
        tmpSettings.add(Integer.toString(ProgramSettings.connectionCountPerDownload));
        tmpSettings.add(ProgramSettings.getDefaultLocation());
        tmpSettings.add(ProgramSettings.getCurrentLookName());
        ReaderWriter.fileWriterCommon(tmpSettings, ProgramSettings.settingsPath);
    }

    public ProgramSettings getProgramSettings() {
        return programSettings;
    }

    public void setProgramSettings(ProgramSettings programSettings) {
        this.programSettings = programSettings;
    }

    public void setNewFilterURL(ArrayList<String> input) {
        this.filterUrl.clear();
        for (int i = 0; i < input.size(); i++) {
            filterUrl.add(input.get(i));
        }
    }

    public void updateDownloadListGUI() {
        if (currentListId == 0)
            currentDownloadList = processingDownloads;
        else if (currentListId == 1)
            currentDownloadList = queueDownloads;
        else if (currentListId == 2)
            currentDownloadList = completedDownloads;
        else if (currentListId == 3)
            currentDownloadList = removedDownloads;
        else if (currentListId == 4)
            currentDownloadList = searchResult;
        mainFrame.getCenterPanel().getDownloadListPanel().updateList(currentDownloadList);
    }

    /**
     *search requested string
     * @param input
     */
    public void search(String input) {
        searchResult.clear();
        for (DownloadItem di : this.processingDownloads) {
            if ((di.getBackend().getMetadata().getFileName().indexOf(input) != -1)
                    || (di.getBackend().getMetadata().getUrl().indexOf(input) != -1)) {
                searchResult.add(di);
            }
        }

        for (DownloadItem di : this.queueDownloads) {
            if ((di.getBackend().getMetadata().getFileName().indexOf(input) != -1)
                    || (di.getBackend().getMetadata().getUrl().indexOf(input) != -1)) {
                searchResult.add(di);
            }
        }

        for (DownloadItem di : this.completedDownloads) {
            if ((di.getBackend().getMetadata().getFileName().indexOf(input) != -1)
                    || (di.getBackend().getMetadata().getUrl().indexOf(input) != -1)) {
                searchResult.add(di);
            }
        }

        for (DownloadItem di : this.removedDownloads) {
            if ((di.getBackend().getMetadata().getFileName().indexOf(input) != -1)
                    || (di.getBackend().getMetadata().getUrl().indexOf(input) != -1)) {
                searchResult.add(di);
            }
        }
        mainFrame.getlPanel().setSelectedPanelBorder(-1);
        this.setCurrentDownloadList(4);
        updateDownloadListGUI();
    }

    /**
     * sort current list
     * @param mode
     */
    public void sort(int mode) {
        if (mode == 0) //sort by name
        {
            Collections.sort(currentDownloadList, new Comparator<DownloadItem>() {
                public int compare(DownloadItem one, DownloadItem other) {
                    return one.getBackend().getMetadata().getFileName().compareTo(other.getBackend().getMetadata().getFileName());
                }
            });
        } else if (mode == 1)//sort by size
        {
            Collections.sort(currentDownloadList, new Comparator<DownloadItem>() {
                public int compare(DownloadItem one, DownloadItem other) {
                    if (one.getBackend().getDownloadSize() >= other.getBackend().getDownloadSize())
                        return 1;
                    else
                        return -1;
                }
            });
        } else if (mode == 2)//sort by date
        {
            Collections.sort(currentDownloadList, new Comparator<DownloadItem>() {
                public int compare(DownloadItem one, DownloadItem other) {
                    return one.getBackend().getMetadata().getStartTime().compareTo(other.getBackend().getMetadata().getStartTime());
                }
            });
        }
        if (currentListId == 0)
            processingSortOrder = 0;
        else if (currentListId == 1)
            queueSortOrder = 0;
        else if (currentListId == 2)
            completedSortOrder = 0;
        else if (currentListId == 3)
            removedSortOrder = 0;
        else if (currentListId == 4)
            searchSortOrder = 0;

        updateDownloadListGUI();
    }

    public void sortOrder(int mode) {
        if (currentDownloadList.size() > 1) {
            if (currentListId == 0) {
                if (mode != processingSortOrder) { // this means need to reverse
                    processingSortOrder = mode;
                    reverseDownloadList();
                    updateDownloadListGUI();
                }
            } else if (currentListId == 1) {
                if (mode != queueSortOrder) { // this means need to reverse
                    queueSortOrder = mode;
                    reverseDownloadList();
                    updateDownloadListGUI();
                }
            } else if (currentListId == 2) {
                if (mode != completedSortOrder) { // this means need to reverse
                    completedSortOrder = mode;
                    reverseDownloadList();
                    updateDownloadListGUI();
                }
            } else if (currentListId == 3) {
                if (mode != removedSortOrder) { // this means need to reverse
                    removedSortOrder = mode;
                    reverseDownloadList();
                    updateDownloadListGUI();
                }
            } else if (currentListId == 4) {
                if (mode != searchSortOrder) { // this means need to reverse
                    searchSortOrder = mode;
                    reverseDownloadList();
                    updateDownloadListGUI();
                }
            }
        }
    }

    public void reverseDownloadList() {
        if (currentDownloadList.size() == 0)
            return;
        int endIndex = currentDownloadList.size() - 1;
        for (int i = 0; i < (currentDownloadList.size() / 2); ++i) {
            DownloadItem tmp = currentDownloadList.get(i);
            currentDownloadList.set(i, currentDownloadList.get(endIndex - i));
            currentDownloadList.set(endIndex - i, tmp);
        }
    }

    public void sortAllList() {
        Collections.sort(processingDownloads, new Comparator<DownloadItem>() {
            public int compare(DownloadItem one, DownloadItem other) {
                return one.getBackend().getMetadata().getStartTime().compareTo(other.getBackend().getMetadata().getStartTime());
            }
        });
        Collections.sort(queueDownloads, new Comparator<DownloadItem>() {
            public int compare(DownloadItem one, DownloadItem other) {
                return one.getBackend().getMetadata().getStartTime().compareTo(other.getBackend().getMetadata().getStartTime());
            }
        });
        Collections.sort(removedDownloads, new Comparator<DownloadItem>() {
            public int compare(DownloadItem one, DownloadItem other) {
                return one.getBackend().getMetadata().getStartTime().compareTo(other.getBackend().getMetadata().getStartTime());
            }
        });
        Collections.sort(completedDownloads, new Comparator<DownloadItem>() {
            public int compare(DownloadItem one, DownloadItem other) {
                return one.getBackend().getMetadata().getStartTime().compareTo(other.getBackend().getMetadata().getStartTime());
            }
        });
        updateDownloadListGUI();
    }

    public void exportData() {
        JFileChooser fc = new JFileChooser();
        String Location = null;
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            Location = fc.getSelectedFile().getAbsolutePath();
        }
        saveAll();
        try {
            FileOutputStream fos = new FileOutputStream(Location + "/jdmData.zip");
            ZipOutputStream zos = new ZipOutputStream(fos);
            addToZipFile(ProgramSettings.processingPath, zos);
            addToZipFile(ProgramSettings.queuePath, zos);
            addToZipFile(ProgramSettings.filterUrlPath, zos);
            addToZipFile(ProgramSettings.settingsPath, zos);
            addToZipFile(ProgramSettings.removePath, zos);
            zos.close();
            fos.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public DownloadPool getProcessingPool() {
        return pool;
    }

    public DownloadPool getQueuePool() {
        return queuePool;
    }

    public void transferToCompleted(DownloadItemBackend e) {
        boolean needRefresh = false;
        if (e.getDownloadListId() == currentListId)
            needRefresh = true;
        if (e.getDownloadListId() == 0) {
            DownloadItem tmpItem = getGUI(e, processingDownloads);
            processingDownloads.remove(tmpItem);
            precessingDownloading.remove(tmpItem);
            completedDownloads.add(tmpItem);
        }
        else if (e.getDownloadListId() == 1) {
            DownloadItem tmpItem = getGUI(e, queueDownloads);
            queueDownloads.remove(tmpItem);
            queueDownloading.remove(tmpItem);
            queuePool.remove(tmpItem);
            completedDownloads.add(tmpItem);
            if (queueWaiting.size() > 0) {
                DownloadItem deq = queueWaiting.get(0);
                queueDownloading.add(deq);
                queuePool.add(deq);
            }
        }
        e.setDownloadListId(2);
        if (needRefresh)
            updateDownloadListGUI();
    }

    public DownloadItem getGUI(DownloadItemBackend e) {
        DownloadItem tmp = null;
        tmp = getGUI(e, processingDownloads);
        if (tmp != null)
            return tmp;
        tmp = getGUI(e, queueDownloads);
        if (tmp != null)
            return tmp;
        tmp = getGUI(e, completedDownloads);
        if (tmp != null)
            return tmp;
        tmp = getGUI(e, removedDownloads);
        if (tmp != null)
            return tmp;
        tmp = getGUI(e, searchResult);
        if (tmp != null)
            return tmp;
        tmp = getGUI(e, currentDownloadList);
        if (tmp != null)
            return tmp;
        return tmp;

    }

    public DownloadItem getGUI(DownloadItemBackend e, ArrayList<DownloadItem> list) {
        DownloadItem tmp = null;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getBackend() == e) {
                tmp = list.get(i);
                break;
            }
        }
        return tmp;
    }

    public boolean acceptUrl(String url) {
        boolean res = true;
        for (String current : filterUrl) {
            int slashIndex = url.indexOf("/");
            slashIndex = url.indexOf("/", slashIndex + 1);
            slashIndex = url.indexOf("/", slashIndex + 1);
            String rowUrl = url.substring(0, slashIndex);
            if (url.equals(current) || rowUrl.indexOf(current) != -1) {
                res = false;
                break;
            }
        }

        return res;
    }

    public void removeFromPool(DownloadItemBackend e) {
        pool.remove(e.getGui(), false);
        queuePool.remove(e.getGui(), false);
    }

    public void setCurrentDownloadList(int index) {
        currentListId = index;
        mainFrame.getCenterPanel().getDownloadListPanel().setCurrentSelectedPanel(-1);
        updateDownloadListGUI();
        mainFrame.getCenterPanel().getDownloadListPanel().setSelectedPanelBorder(-1);
    }

}

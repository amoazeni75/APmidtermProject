package Models;

import javax.swing.*;

/**
 * this class contains programme settings
 */
public class ProgramSettings {

    public static int parallelDownloadCount;
    public static int connectionCountPerDownload;
    public static int eachDownloadWorkerCount;
    public static String defaultLocation;
    public static UIManager.LookAndFeelInfo[] lookAndFeels;
    public static UIManager.LookAndFeelInfo currentLook;
    public static String LookAndFeelName;
    public static String processingPath;
    public static String queuePath;
    public static String removePath;
    public static String settingsPath;
    public static String finalRemovePath;
    public static String filterUrlPath;
    public static String TEMP_DIRECTORY;
    public static String completedPath;

    public ProgramSettings() throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        parallelDownloadCount = 200;
        connectionCountPerDownload = 1;
        defaultLocation = javax.swing.filechooser.FileSystemView.getFileSystemView().getHomeDirectory().getAbsolutePath();
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        lookAndFeels = UIManager.getInstalledLookAndFeels();
        for (int i = 0; i < lookAndFeels.length; i++) {
            if(lookAndFeels[i].getClassName().equals(UIManager.getSystemLookAndFeelClassName())) {
                currentLook = lookAndFeels[i];
                LookAndFeelName = lookAndFeels[i].getName();
            }
        }
        eachDownloadWorkerCount = 2;
        TEMP_DIRECTORY = "./tempPart/";
        processingPath = "./ApplicationData/list.jdm";
        queuePath = "./ApplicationData/queue.jdm";
        removePath = "./ApplicationData/removed.jdm";
        settingsPath = "./ApplicationData/settings.jdm";
        finalRemovePath = "./ApplicationData/removed.txt";
        filterUrlPath = "./ApplicationData/filter.jdm";
        completedPath = "./ApplicationData/completed.jdm";
    }

    public static String getDefaultLocation() {
        return defaultLocation;
    }

    public void setDefaultLocation(String defaultLocation) {
        ProgramSettings.defaultLocation = defaultLocation;
    }

    public static UIManager.LookAndFeelInfo getLookAndFeels(int i) {
        return lookAndFeels[i];
    }

    public static UIManager.LookAndFeelInfo getCurrentLook() {
        return currentLook;
    }

    public void setCurrentLook(UIManager.LookAndFeelInfo currentLook) {
        ProgramSettings.currentLook = currentLook;
    }

    public static String getCurrentLookName() {
        return currentLook.getName();
    }

    public int getParallelDownloadCount() {
        return parallelDownloadCount;
    }

    public int getLookAndFeelsCount(){
        return lookAndFeels.length;
    }

    public void setParallelDownloadCount(int parallelDownloadCount) {
        ProgramSettings.parallelDownloadCount = parallelDownloadCount;
    }

    public void setLookAndFeels(UIManager.LookAndFeelInfo[] lookAndFeels) {
        ProgramSettings.lookAndFeels = lookAndFeels;
    }

    public void setLookAndFeel(String look) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {

        for (int i = 0; i < lookAndFeels.length; i++) {
            if(lookAndFeels[i].getName().equals(look)) {
                currentLook = lookAndFeels[i];
                UIManager.setLookAndFeel(lookAndFeels[i].getClassName());
                LookAndFeelName = lookAndFeels[i].getName();
            }
        }
    }

    public static int getConnectionCountPerDownload() {
        return connectionCountPerDownload;
    }

    public static void setConnectionCountPerDownload(int connectionCountPerDownload) {
        ProgramSettings.connectionCountPerDownload = connectionCountPerDownload;
    }
}

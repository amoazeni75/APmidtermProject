package Models;

import GUI.DownloadItem;
import GUI.MainPanel;

import java.util.ArrayList;
/**
 * this interface define some action on MainTaskPanel panel
 */
public interface MainFrameTask {
    void updateFrame();
    void switchUI(int index);
}

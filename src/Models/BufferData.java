package Models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * this class is about buffer size data
 */
public class BufferData implements Serializable {
   private String startTime;
   private String endTime;
   private String status;
   private String completed;
   private String url;
   private String name;
   private String type;
   private String size;
   private String path;
   private String downloadListId;
   private String id;
   private String rangeAllowed;

   private ArrayList<BufferPart> parts;

   public BufferData(){
       parts = new ArrayList<>();
   }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRangeAllowed() {
        return rangeAllowed;
    }

    public void setRangeAllowed(String rangeAllowed) {
        this.rangeAllowed = rangeAllowed;
    }

    public String getDownloadListId() {
        return downloadListId;
    }

    public void setDownloadListId(String downloadListId) {
        this.downloadListId = downloadListId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;

    }

    public ArrayList<DownloadPartsMetadata> getParts() {
        ArrayList<DownloadPartsMetadata> res = new ArrayList<>();
        for (int i = 0; i < parts.size(); i++) {
            DownloadPartsMetadata mm = new DownloadPartsMetadata(parts.get(i).getDownloadid(),parts.get(i).getId(),
                    parts.get(i).getStart(),parts.get(i).getEnd(),parts.get(i).getPath());
            res.add(mm);
        }
        return res;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void addPart(BufferPart bf){
       parts.add(bf);
    }
}

package Models;

import java.io.Serializable;

/**
 * this class simulated buffer part size for multi thread download
 */
public class BufferPart implements Serializable {
    private int id;
    private long downloadid;
    private long start;
    private long end;
    private String path;
    public BufferPart(long downloadid,int id,long s,long e, String p){
        this.id = id;
        start = s;
        end = e;
        path = p;
        this.downloadid = downloadid;
    }

    public long getDownloadid() {
        return downloadid;
    }

    public int getId() {
        return id;
    }

    public long getStart() {
        return start;
    }

    public long getEnd() {
        return end;
    }

    public String getPath() {
        return path;
    }
}

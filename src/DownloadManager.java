import GUI.MainFrame;

import javax.swing.*;
import java.io.IOException;

public class DownloadManager {
    private MainFrame mainFrame;

    public DownloadManager() {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new MainFrame().showGUI();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (UnsupportedLookAndFeelException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

//
//        mainFrame = new MainFrame();
//        mainFrame.showGUI();
    }
}
